# MrTracker
Engine to merge and calibrate multiple tracking technologies

## Build from sources
1. Download Projucer
2. Open AugmentaFusion.jucer
3. Hit "Save and Open in IDE"
4. Compile
5. Enjoy.

## Requirement

OpenVR SDK : https://github.com/ValveSoftware/openvr

hint : You need to copy openvr_api.dll next to the binary

/* =========================================================================================

   This is an auto-generated file: Any edits you make may be overwritten!

*/

#pragma once

namespace BinaryData
{
    extern const char*   add_png;
    const int            add_pngSize = 16103;

    extern const char*   cancel_png;
    const int            cancel_pngSize = 14817;

    extern const char*   check_png;
    const int            check_pngSize = 15786;

    extern const char*   default_mrlayout;
    const int            default_mrlayoutSize = 1923;

    extern const char*   edit_png;
    const int            edit_pngSize = 16204;

    extern const char*   file_png;
    const int            file_pngSize = 16030;

    extern const char*   icon_png;
    const int            icon_pngSize = 78197;

    extern const char*   in_png;
    const int            in_pngSize = 20741;

    extern const char*   out_png;
    const int            out_pngSize = 20427;

    extern const char*   power_png;
    const int            power_pngSize = 15366;

    extern const char*   reload_png;
    const int            reload_pngSize = 16303;

    extern const char*   settings_png;
    const int            settings_pngSize = 15919;

    extern const char*   target_png;
    const int            target_pngSize = 15244;

    extern const char*   uncheck_png;
    const int            uncheck_pngSize = 15619;

    // Number of elements in the namedResourceList and originalFileNames arrays.
    const int namedResourceListSize = 14;

    // Points to the start of a list of resource names.
    extern const char* namedResourceList[];

    // Points to the start of a list of resource filenames.
    extern const char* originalFilenames[];

    // If you provide the name of one of the binary resource variables above, this function will
    // return the corresponding data and its size (or a null pointer if the name isn't found).
    const char* getNamedResource (const char* resourceNameUTF8, int& dataSizeInBytes);

    // If you provide the name of one of the binary resource variables above, this function will
    // return the corresponding original, non-mangled filename (or a null pointer if the name isn't found).
    const char* getNamedResourceOriginalFilename (const char* resourceNameUTF8);
}

/*
  ==============================================================================

    SharedMemoryOutput.cpp
    Created: 23 Feb 2017 12:13:18pm
    Author:  Ben

  ==============================================================================
*/

#include "SharedMemoryOutput.h"

SharedMemoryOutput::SharedMemoryOutput(const String & name) :
	Output(name)
{

}

void SharedMemoryOutput::onContainerParameterChangedInternal(Parameter * p)
{
	Output::onContainerParameterChangedInternal(p);
}


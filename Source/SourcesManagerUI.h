/*
  ==============================================================================

    SourcesManagerUI.h
    Created: 23 Feb 2017 10:56:26am
    Author:  Ben

  ==============================================================================
*/

#ifndef SOURCESMANAGERUI_H_INCLUDED
#define SOURCESMANAGERUI_H_INCLUDED

#include "BaseManagerShapeShifterUI.h"
#include "SourcesManager.h"
#include "SourceUI.h"

class SourcesManagerUI :
	public BaseManagerShapeShifterUI<SourcesManager, Source, SourceUI>
{
public:
	SourcesManagerUI(const String &name, SourcesManager * manager);
	~SourcesManagerUI();

	void showMenuAndAddItem(bool, Point<int>) override;

	JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(SourcesManagerUI)
};


#endif  // SOURCESMANAGERUI_H_INCLUDED

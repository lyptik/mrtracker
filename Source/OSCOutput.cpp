/*
  ==============================================================================

    OSCOutput.cpp
    Created: 23 Feb 2017 12:13:14pm
    Author:  Ben

  ==============================================================================
*/

#include "OSCOutput.h"
#include "SourcesManager.h"

OSCOutput::OSCOutput(const String & name, int defaultRemotePort) :
	Output(name)
{
	//Send


	useLocal = addBoolParameter("Local", "Send to Local IP (127.0.0.1). Allow to quickly switch between local and remote IP.", true);
	remoteHost = addStringParameter("Remote Host", "Remote Host to send to.", "127.0.0.1");
	remotePort = addIntParameter("Remote port", "Port on which the remote host is listening to", defaultRemotePort, 1024, 65535);

	localPort = addIntParameter("Local Port", "Local Port to bind to receive OSC Messages", 11450, 1024, 65535);
	localPort->hideInOutliner = true;
	localPort->isTargettable = false;

	setupSender();
	setupReceiver();
}


void OSCOutput::setupReceiver()
{
	bool result = receiver.connect(localPort->intValue());

	if (result)
	{
		NLOG(niceName, "Now receiving on port : " + localPort->stringValue());
	}
	else
	{
		NLOG(niceName, "Error binding port " + localPort->stringValue());
	}

	Array<IPAddress> ad;
	IPAddress::findAllAddresses(ad);

	String s = "Local IPs:";
	for (auto &a : ad) s += String("\n > ") + a.toString();
	NLOG(niceName, s);
}

void OSCOutput::setupSender()
{
	sender.disconnect();
	sender.connect(remoteHost->stringValue(), remotePort->intValue());
	NLOG(niceName,"Now sending to " << remoteHost->stringValue() << ":" << remotePort->intValue());
}

void OSCOutput::sendOSC(const OSCMessage & msg)
{
	if (logOutgoingData->boolValue())
	{
		NLOG(niceName, "Send OSC : " << msg.getAddressPattern().toString());
	}

	sender.sendToIPAddress(remoteHost->stringValue(),remotePort->intValue(),msg);
}

void OSCOutput::sendTrackableData(Trackable * t)
{
	sendTrackableMessage(t);
	if (t->source->hasPosition) sendPositionMessage(t);
	if (t->source->hasCentroid) sendCentroidMessage(t);
	if (t->source->hasRotation) sendRotationMessage(t);
	if (t->source->hasSize) sendSizeMessage(t);
}

void OSCOutput::sendTrackableMessage(Trackable * t)
{
	OSCMessage m("/trackable");
	m.addInt32(t->data.id);
	m.addInt32(t->data.type);
	m.addInt32(t->source->sourceTypeID);
	sendOSC(m);
}

void OSCOutput::sendPositionMessage(Trackable * t)
{
	OSCMessage m("/trackable/position");
	m.addInt32(t->data.id);
	m.addFloat32(t->data.x);
	m.addFloat32(t->data.y);
	m.addFloat32(t->data.z);
	sendOSC(m);
}

void OSCOutput::sendRotationMessage(Trackable * t)
{
	OSCMessage om("/trackable/rotation");
	om.addInt32(t->data.id);
	om.addFloat32(t->data.ox);
	om.addFloat32(t->data.oy);
	om.addFloat32(t->data.oz);
	sendOSC(om);

	OSCMessage qm("/trackable/quaternion");
	qm.addInt32(t->data.id);
	qm.addFloat32(t->data.qx);
	qm.addFloat32(t->data.qy);
	qm.addFloat32(t->data.qz);
	qm.addFloat32(t->data.qw);
	sendOSC(qm);
}

void OSCOutput::sendCentroidMessage(Trackable * t)
{
	OSCMessage m("/trackable/centroid");
	m.addInt32(t->data.id);
	m.addFloat32(t->data.cx);
	m.addFloat32(t->data.cy);
	m.addFloat32(t->data.cz);
	sendOSC(m);
}

void OSCOutput::sendSizeMessage(Trackable * t)
{
	OSCMessage m("/trackable/size");
	m.addInt32(t->data.id);
	m.addFloat32(t->data.sx);
	m.addFloat32(t->data.sy);
	m.addFloat32(t->data.sz);
	sendOSC(m);
}



void OSCOutput::sendButtonTouchData(Trackable * t, int buttonID, bool value)
{
	OSCMessage m("/trackable/touch");
	m.addInt32(t->data.id);
	m.addInt32(buttonID);
	m.addFloat32(value);
	sendOSC(m);
}

void OSCOutput::sendButtonPressData(Trackable * t, int buttonID, bool value)
{
	OSCMessage m("/trackable/press");
	m.addInt32(t->data.id);
	m.addInt32(buttonID);
	m.addFloat32(value);
	sendOSC(m);
}

void OSCOutput::sendAxisData(Trackable * t, int axisID, float valueX, float valueY)
{
	OSCMessage m("/trackable/axis");
	m.addInt32(t->data.id);
	m.addInt32(axisID);
	m.addFloat32(valueX);
	m.addFloat32(valueY);
	sendOSC(m);
}

void OSCOutput::onContainerParameterChangedInternal(Parameter * p)
{
	Output::onContainerParameterChangedInternal(p);
	if (p == remoteHost || p == remotePort)
	{
		setupSender();
	}
	else if (p == localPort)
	{
		setupReceiver();
	}
}

void OSCOutput::processMessage(const OSCMessage & msg)
{
	String address = msg.getAddressPattern().toString();
	if (address == "/vibrate")
	{
		if (msg.size() >= 3)
		{
			int controllerId = msg[0].getInt32();
			float strength = msg[1].getFloat32();
			float duration = msg[2].getFloat32();
			SourcesManager::getInstance()->vibrateController(controllerId,strength, duration);
		}
	}
}

void OSCOutput::oscMessageReceived(const OSCMessage & message)
{
	if (!enabled->boolValue()) return;
	processMessage(message);
}

void OSCOutput::oscBundleReceived(const OSCBundle & bundle)
{
	if (!enabled->boolValue()) return;
	for (auto &m : bundle)
	{
		processMessage(m.getMessage());
	}
}

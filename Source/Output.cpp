/*
  ==============================================================================

    Output.cpp
    Created: 23 Feb 2017 10:55:36am
    Author:  Ben

  ==============================================================================
*/

#include "Output.h"

Output::Output(const String & name) :
	BaseItem(name)
{
	logOutgoingData = addBoolParameter("Log Outgoing", "Enable / Disable logging of outgoing data for this module", false);
	logOutgoingData->hideInOutliner = true;
	logOutgoingData->isTargettable = false;
}

Output::~Output()
{
}


void Output::sendTrackableData(Trackable *)
{
	//to override
}

void Output::sendButtonTouchData(Trackable * , int , bool )
{
	//to override
}

void Output::sendButtonPressData(Trackable * , int , bool )
{
	//to override
}

void Output::sendAxisData(Trackable * , int , float , float )
{
	//to override
}

var Output::getJSONData()
{
	var data = BaseItem::getJSONData();
	data.getDynamicObject()->setProperty("type", getTypeString());
	return data;
}

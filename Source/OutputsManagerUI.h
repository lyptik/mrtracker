/*
  ==============================================================================

    OutputsManagerUI.h
    Created: 23 Feb 2017 10:56:32am
    Author:  Ben

  ==============================================================================
*/

#ifndef OUTPUTSMANAGERUI_H_INCLUDED
#define OUTPUTSMANAGERUI_H_INCLUDED


#include "BaseManagerShapeShifterUI.h"
#include "OutputsManager.h"
#include "OutputUI.h"

class OutputsManagerUI :
	public BaseManagerShapeShifterUI<OutputsManager, Output, OutputUI>
{
public:
	OutputsManagerUI(const String &name, OutputsManager * manager);
	~OutputsManagerUI();


	void showMenuAndAddItem(bool, Point<int>) override;

	JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(OutputsManagerUI)
};




#endif  // OUTPUTSMANAGERUI_H_INCLUDED

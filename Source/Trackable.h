/*
  ==============================================================================

    Trackable.h
    Created: 23 Feb 2017 11:21:03am
    Author:  Ben

  ==============================================================================
*/

#ifndef TRACKABLE_H_INCLUDED
#define TRACKABLE_H_INCLUDED

#include "BaseItem.h"
#include "TrackableData.h"
#include "J3D/Transform.h"

class Source;

class Trackable :
	public BaseItem,
	public Transform
{
public:
	Trackable(Source * source = nullptr, uint8 id = -1, const String &name = "Trackable");
	~Trackable();

	TrackableData data;
	Source * source;

	//Point3DParameter * position;
	//Point3DParameter * rotation;
	BoolParameter * isDetected;


	void updatePosition(float x, float y, float z, bool silent = true);
	void updateRotation(float x, float y, float z, bool silent = true);
	virtual void updateQuaternion(float x, float y, float z, float w, bool silent = true);
	void updateSize(float x, float y, float z, bool silent = true);
	void updateCentroid(float x, float y, float z, bool silent = true);

	virtual void handleButtonTouch(int buttonID, bool touch);
	virtual void handleButtonPress(int buttonID, bool pressed);
	virtual void handleAxisChange(int axisID, float valueX, float valueY);

	void dispatchDataUpdate();
	void dispatchButtonTouch(int buttonID, bool value);
	void dispatchButtonPress(int buttonID, bool value);
	void dispatchAxisUpdate(int axisID, float valueX, float valueY);


	virtual void vibrate(float /*strength*/, float /*time*/) {}

	Vector3D<float> getAbsolutePosition();
	Vector3D<float> getAbsoluteRotation();

	class TrackableListener {
	public:
		virtual ~TrackableListener() {};
		virtual void dataUpdate(Trackable *) {};
		virtual void buttonTouch(Trackable *, int, bool) {};
		virtual void buttonPress(Trackable *, int, bool) {};
		virtual void axisUpdate(Trackable *, int, float, float) {};
	};

	ListenerList<TrackableListener> trackableListeners;
	void addTrackableListener(TrackableListener* e) { trackableListeners.add(e); }
	void removeTrackableListener(TrackableListener* e) { trackableListeners.remove(e); }



	class  TrackableEvent {
	public:
		enum Type { DATA_UPDATE, DETECTION_UPDATE };

		TrackableEvent(Trackable * trackable, Type t) : type(t), trackable(trackable) {}
		Type type;
		Trackable * trackable;

	};
	QueuedNotifier<TrackableEvent> queuedNotifier;
	typedef QueuedNotifier<TrackableEvent>::Listener AsyncListener;


	void addAsyncTrackableListener(AsyncListener* newListener) { queuedNotifier.addListener(newListener); }
	void addAsyncCoalescedTrackableListener(AsyncListener* newListener) { queuedNotifier.addAsyncCoalescedListener(newListener); }
	void removeAsyncTrackableListener(AsyncListener* listener) { queuedNotifier.removeListener(listener); }

	
	JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(Trackable)
};



#endif  // TRACKABLE_H_INCLUDED

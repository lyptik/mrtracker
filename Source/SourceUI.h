/*
  ==============================================================================

	SourceUI.h
	Created: 23 Feb 2017 11:01:53am
	Author:  Ben

  ==============================================================================
*/

#ifndef SOURCEUI_H_INCLUDED
#define SOURCEUI_H_INCLUDED

#include "BaseItemUI.h"
#include "Source.h"

class SourceUI :
	public BaseItemUI<Source>
{
public:
	SourceUI(Source * source);
	~SourceUI();

	void resizedInternalHeader(Rectangle<int> &r) override;

	ScopedPointer<TriggerImageUI> inActivityUI;


	JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(SourceUI)
};



#endif  // SOURCEUI_H_INCLUDED

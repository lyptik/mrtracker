/*
  ==============================================================================

    SceneView2D.h
    Created: 23 Feb 2017 10:56:05am
    Author:  Ben

  ==============================================================================
*/

#ifndef SCENEVIEW2D_H_INCLUDED
#define SCENEVIEW2D_H_INCLUDED

#include "SourcesManager.h"
#include "SourceView2DUI.h"
#include "BaseManagerShapeShifterUI.h"

class SceneView2D :
	public BaseManagerShapeShifterUI<SourcesManager,Source,SourceView2DUI>,
	public SourceView2DUI::SourceView2DUIListener
{
public:
	SceneView2D();
	~SceneView2D();

	float pixelsPerUnit = 32; //for potential zoom
	
	Point<int> viewOffset; //in pixels, viewOffset of 0 means zeroPos is at the center of the window
	Point<int> initViewOffset;

	//Mouse interfaction for source2DUI
	Vector3D<float> initValue; //can be for origin, rotation or scale

	void mouseDown(const MouseEvent &e) override;
	void mouseDrag(const MouseEvent &e) override;
	void mouseUp(const MouseEvent &e) override;
	void mouseWheelMove(const MouseEvent &e, const MouseWheelDetails &d) override;
	bool keyPressed(const KeyPress &e) override;
	
	void paint(Graphics &g) override;
	void resized() override;
	void updateSourceUIBounds(SourceView2DUI * sui);
	
	void setZoom(float ppu);

	void homeView();
	Point<int> getSize();
	Point<float> getViewMousePosition();
	Point<float> getViewPos(const Point<int> &posInView);
	Point<int> getPosInView(const Point<float> &originPos);

	void addItemUIInternal(SourceView2DUI * sui) override;
	void removeItemUIInternal(SourceView2DUI * sui) override;
	
	void sourceBoundsParamChanged(SourceView2DUI * sui) override;

};



#endif  // SCENEVIEW2D_H_INCLUDED

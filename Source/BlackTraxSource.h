/*
  ==============================================================================

    BlackTraxSource.h
    Created: 23 Feb 2017 12:08:44pm
    Author:  Ben

  ==============================================================================
*/

#ifndef BLACKTRAXSOURCE_H_INCLUDED
#define BLACKTRAXSOURCE_H_INCLUDED

#include "Source.h"

class BlacktraxSource :
	public Source
{
public:
	BlacktraxSource(const String &name = "Blacktrax");
	~BlacktraxSource() {}

	static BlacktraxSource * create() { return new BlacktraxSource(); }
	virtual String getTypeString() const override { return "Blacktrax"; }
};





#endif  // BLACKTRAXSOURCE_H_INCLUDED

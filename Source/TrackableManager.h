/*
  ==============================================================================

    TrackableManager.h
    Created: 23 Feb 2017 12:24:52pm
    Author:  Ben

  ==============================================================================
*/

#ifndef TRACKABLEMANAGER_H_INCLUDED
#define TRACKABLEMANAGER_H_INCLUDED

#include "Trackable.h"
#include "BaseManager.h"

class TrackableManager :
	public BaseManager<Trackable>
{
public:
	TrackableManager(Source * source);
	~TrackableManager();

	Source * source;
	Trackable * createItem() override;
	
	Trackable * addItemWithID(uint8 id);
	void removeItemWithID(uint8 id);
	Trackable * getItemWithID(uint8 id);

	JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(TrackableManager)
};



#endif  // TRACKABLEMANAGER_H_INCLUDED

/*
  ==============================================================================

    IntStepperUI.cpp
    Created: 8 Mar 2016 3:46:43pm
    Author:  bkupe

  ==============================================================================
*/

#include "IntStepperUI.h"

IntStepperUI::IntStepperUI(Parameter * parameter) :
    FloatStepperUI(parameter)
{
}

IntStepperUI::~IntStepperUI()
{
}

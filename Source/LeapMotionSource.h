/*
  ==============================================================================

    LeapMotionSource.h
    Created: 23 Feb 2017 12:09:05pm
    Author:  Ben

  ==============================================================================
*/

#ifndef LEAPMOTIONSOURCE_H_INCLUDED
#define LEAPMOTIONSOURCE_H_INCLUDED


#include "Source.h"

class LeapMotionSource :
	public Source
{
public:
	LeapMotionSource(const String &name = "LeapMotion");
	~LeapMotionSource() {}

	static LeapMotionSource * create() { return new LeapMotionSource(); }
	virtual String getTypeString() const override { return "LeapMotion"; }
};





#endif  // LEAPMOTIONSOURCE_H_INCLUDED

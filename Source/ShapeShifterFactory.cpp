/*
  ==============================================================================

    ShapeShifterFactory.cpp
    Created: 18 May 2016 11:45:57am
    Author:  bkupe

  ==============================================================================
*/

#include "ShapeShifterFactory.h"

#include "Inspector.h"
#include "CustomLoggerUI.h"
#include "SourcesManagerUI.h"
#include "OutputsManagerUI.h"
#include "SceneView2D.h"

#include "MainComponent.h"

ShapeShifterContent * ShapeShifterFactory::createContentForIndex(PanelName pn)
{
	String contentName = globalPanelNames[(int)pn];

	switch (pn)
	{

	case InspectorPanel:
		return new InspectorUI(true); //Inspector created by Panels is set to main inspector and is assured unique by the ShapeShifterManager panel handling
		break;

	case LoggerPanel:
		return new CustomLoggerUI(contentName, CustomLogger::getInstance());
		break;

	case SourcesPanel:
		return new SourcesManagerUI(contentName, SourcesManager::getInstance());
		break;

	case OutputsPanel:
		return new OutputsManagerUI(contentName, OutputsManager::getInstance());
		break;

	case View2DPanel :
		return new SceneView2D();
		break;

	default:
		DBG("Panel not handled : " << contentName << ", index in names = " << globalPanelNames.strings.indexOf(contentName));
	}
	return nullptr;
}

ShapeShifterContent * ShapeShifterFactory::createContentForName(const String &name)
{
	return createContentForIndex((PanelName)globalPanelNames.strings.indexOf(name));
}

/*
  ==============================================================================

    KinectSource.h
    Created: 23 Feb 2017 12:08:55pm
    Author:  Ben

  ==============================================================================
*/

#ifndef KINECTSOURCE_H_INCLUDED
#define KINECTSOURCE_H_INCLUDED

#include "Source.h"

class KinectSource :
	public Source
{
public:
	KinectSource(const String &name = "Kinect");
	~KinectSource() {}

	static KinectSource * create() { return new KinectSource(); }
	virtual String getTypeString() const override { return "Kinect"; }
};






#endif  // KINECTSOURCE_H_INCLUDED

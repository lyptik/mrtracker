/*
  ==============================================================================

    SourceView2DUI.h
    Created: 23 Feb 2017 4:49:28pm
    Author:  Ben

  ==============================================================================
*/

#ifndef SOURCEVIEW2DUI_H_INCLUDED
#define SOURCEVIEW2DUI_H_INCLUDED

#include "BaseItemMinimalUI.h"
#include "Source.h"
#include "TrackableManagerViewUI.h"

class SourceView2DUI :
	public BaseItemMinimalUI<Source>
{
public:
	SourceView2DUI(Source * source);
	~SourceView2DUI();

	Source * source;
	ScopedPointer<TrackableManagerViewUI> trackableManagerUI;

	void paint(Graphics &g) override;
	void resized() override;

	void mouseDown(const MouseEvent &e) override;
	void mouseDrag(const MouseEvent &e) override;
	


	void controllableFeedbackUpdateInternal(Controllable *) override;

	class SourceView2DUIListener {
	public:
		virtual ~SourceView2DUIListener() {};

		virtual void sourceBoundsParamChanged(SourceView2DUI *) {};
	};

	ListenerList<SourceView2DUIListener> uiListeners;
	void addSourceViewListener(SourceView2DUIListener* e) { uiListeners.add(e); }
	void removeSourceViewListener(SourceView2DUIListener* e) { uiListeners.remove(e); }

	
};



#endif  // SOURCEVIEW2DUI_H_INCLUDED

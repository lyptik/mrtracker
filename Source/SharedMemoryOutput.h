/*
  ==============================================================================

    SharedMemoryOutput.h
    Created: 23 Feb 2017 12:13:18pm
    Author:  Ben

  ==============================================================================
*/

#ifndef SHAREDMEMORYOUTPUT_H_INCLUDED
#define SHAREDMEMORYOUTPUT_H_INCLUDED


#include "Output.h"

class SharedMemoryOutput :
	public Output
{
public:
	SharedMemoryOutput(const String &name = "SharedMemory");
	~SharedMemoryOutput() {}

	virtual void onContainerParameterChangedInternal(Parameter * p) override;

	static SharedMemoryOutput * create() { return new SharedMemoryOutput(); }
	virtual String getTypeString() const override { return "SharedMemory"; }

};




#endif  // SHAREDMEMORYOUTPUT_H_INCLUDED

/*
  ==============================================================================

    ViveSource.h
    Created: 23 Feb 2017 11:19:12am
    Author:  Ben

  ==============================================================================
*/

#ifndef VIVESOURCE_H_INCLUDED
#define VIVESOURCE_H_INCLUDED


#include "Source.h"
#include "openvr.h"

using namespace vr;

#define VIVE_SOURCE_ID 0x02

class ViveSource :
	public Source,
	public Thread
{
public:
	ViveSource(const String &name = "Vive");
	~ViveSource();

	BoolParameter * defaultAlternateOrientation;

	IVRSystem * vrSystem;
	BoolParameter * hmdPresent;
	

	HashMap<int, Trackable *> trackableMap;
	TrackedDevicePose_t poses[k_unMaxTrackedDeviceCount];

	void addTrackableAtIndex(int vrTrackedDeviceIndex);
	Trackable * getTrackableItemIndex(int vrTrackedDeviceIndex, bool createIfNotThere);

	Matrix3D<float> getMatrix3DForHMDMatrix(HmdMatrix34_t m);
	void getRotationFromMatrix(HmdMatrix34_t m, float &yaw, float &pitch, float &roll);
	void getQuaternionFromMatrix(HmdMatrix34_t m, float &x, float &y, float &z, float &w);
	static ViveSource * create() { return new ViveSource(); }
	virtual String getTypeString() const override { return "Vive"; }

	Trackable * getTrackableWithID(int tid);

	void vibrateController(int controllerID, float strength, float time) override;
	void vibrateController(int controllerID, float strength);

	// H�rit� via Thread
	virtual void run() override;
};





#endif  // VIVESOURCE_H_INCLUDED

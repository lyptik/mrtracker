/*
  ==============================================================================

    MrTrackerSource.cpp
    Created: 23 Feb 2017 11:19:23am
    Author:  Ben

  ==============================================================================
*/

#include "MrTrackerSource.h"


MrTrackerSource::MrTrackerSource(const String & name, int defaultLocalPort) :
	Source(name,3)
{
	//Receive
	localPort = addIntParameter("Local Port", "Local Port to bind to receive OSC Messages", defaultLocalPort, 1024, 65535);
	localPort->hideInOutliner = true;
	localPort->isTargettable = false;

	isConnected = addBoolParameter("Is Connected", "Is the receiver bound the the local port", false);
	isConnected->isEditable = false;
	isConnected->hideInOutliner = true;
	isConnected->isTargettable = false;
	isConnected->isSavable = false;

	setupReceiver();
}

void MrTrackerSource::setupReceiver()
{
	receiver = new DatagramSocket();
	bool result = receiver->bindToPort(localPort->intValue());
	isConnected->setValue(result);
}

void MrTrackerSource::onContainerParameterChangedInternal(Parameter * p)
{
	Source::onContainerParameterChangedInternal(p);
	if (p == localPort) setupReceiver();
}

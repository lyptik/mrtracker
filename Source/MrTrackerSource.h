/*
  ==============================================================================

    MrTrackerSource.h
    Created: 23 Feb 2017 11:19:23am
    Author:  Ben

  ==============================================================================
*/

#ifndef MRTRACKERSOURCE_H_INCLUDED
#define MRTRACKERSOURCE_H_INCLUDED


#include "Source.h"

class MrTrackerSource :
	public Source
{
public:
	MrTrackerSource(const String &name = "MrTracker", int defaultLocalPort = 16500);
	~MrTrackerSource() {}

	//RECEIVE
	IntParameter * localPort;
	BoolParameter * isConnected;
	ScopedPointer<DatagramSocket> receiver;

	//RECEIVE
	void setupReceiver();


	virtual void onContainerParameterChangedInternal(Parameter * p) override;

	static MrTrackerSource * create() { return new MrTrackerSource(); }
	virtual String getTypeString() const override { return "MrTracker"; }
};



#endif  // MRTRACKERSOURCE_H_INCLUDED

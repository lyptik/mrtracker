/*
  ==============================================================================

    SourceFactory.cpp
    Created: 23 Feb 2017 11:00:25am
    Author:  Ben

  ==============================================================================
*/

#include "SourceFactory.h"
#include "AugmentaSource.h"
#include "MrTrackerSource.h"
#include "ViveSource.h"
#include "KinectSource.h"
#include "LeapMotionSource.h"
#include "BlackTraxSource.h"
#include "OptitrackSource.h"

juce_ImplementSingleton(SourceFactory)

SourceFactory::SourceFactory() {

	sourceDefs.add(new SourceDefinition("", "Augmenta", &AugmentaSource::create));
	sourceDefs.add(new SourceDefinition("", "Vive", &ViveSource::create));
	sourceDefs.add(new SourceDefinition("", "MrTracker", &MrTrackerSource::create));
	sourceDefs.add(new SourceDefinition("", "Blacktrax", &BlacktraxSource::create));
	sourceDefs.add(new SourceDefinition("", "Optitrack", &OptitrackSource::create));
	sourceDefs.add(new SourceDefinition("", "Kinect", &KinectSource::create));
	sourceDefs.add(new SourceDefinition("", "LeapMotion", &LeapMotionSource::create));


	buildPopupMenu();
}

void SourceFactory::buildPopupMenu()
{
	OwnedArray<PopupMenu> subMenus;
	Array<String> subMenuNames;

	for (auto &d : sourceDefs)
	{
		int itemID = sourceDefs.indexOf(d) + 1;//start at 1 for menu

		if (d->menuPath.isEmpty())
		{
			menu.addItem(itemID, d->sourceType);
			continue;
		}

		int subMenuIndex = -1;

		for (int i = 0; i < subMenus.size(); i++)
		{
			if (subMenuNames[i] == d->menuPath)
			{
				subMenuIndex = i;
				break;
			}
		}
		if (subMenuIndex == -1)
		{
			subMenuNames.add(d->menuPath);
			subMenus.add(new PopupMenu());
			subMenuIndex = subMenus.size() - 1;
		}

		subMenus[subMenuIndex]->addItem(itemID, d->sourceType);
	}

	for (int i = 0; i < subMenus.size(); i++) menu.addSubMenu(subMenuNames[i], *subMenus[i]);
}

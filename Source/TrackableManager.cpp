/*
  ==============================================================================

    TrackableManager.cpp
    Created: 23 Feb 2017 12:24:52pm
    Author:  Ben

  ==============================================================================
*/

#include "TrackableManager.h"


TrackableManager::TrackableManager(Source * _source) :
	BaseManager("Trackables"),
	source(_source)
{
	selectItemWhenCreated = false;
}

TrackableManager::~TrackableManager()
{
}

Trackable * TrackableManager::createItem()
{
	return new Trackable(source);
}

Trackable * TrackableManager::addItemWithID(uint8 id)
{
	Trackable * t = getItemWithID(id);
	if (t == nullptr)
	{
		t = new Trackable(source, id);
		addItem(t);
	}

	return t;
}

void TrackableManager::removeItemWithID(uint8 id)
{
	Trackable * t = getItemWithID(id);
	if (t == nullptr) return;
	removeItem(t);
}

Trackable * TrackableManager::getItemWithID(uint8 id)
{
	for (auto &i : items)
	{
		if (i->data.id == id) return i;
	}
	return nullptr;
}

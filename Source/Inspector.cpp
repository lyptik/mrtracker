/*
  ==============================================================================

    Inspector.cpp
    Created: 9 May 2016 6:41:38pm
    Author:  bkupe

  ==============================================================================
*/

#include "Inspector.h"

Inspector::Inspector(bool isMain) :
	currentInspectable(nullptr),
	currentEditor(nullptr),
	isMainInspector(isMain)
{
	
	if (InspectableSelectionManager::getInstanceWithoutCreating() != nullptr) InspectableSelectionManager::getInstance()->addSelectionListener(this);

	vp.setScrollBarsShown(true, false);
	vp.setScrollOnDragEnabled(false);
	vp.setScrollBarThickness(10);
	addAndMakeVisible(vp);

	resized();
}


Inspector::~Inspector()
{
	if (InspectableSelectionManager::getInstanceWithoutCreating() != nullptr) InspectableSelectionManager::getInstance()->removeSelectionListener(this); 
	clear();
}

void Inspector::resized()
{
	Rectangle<int> r = getLocalBounds().reduced(3);
	vp.setBounds(r);
	r.removeFromRight(10);

	if (currentEditor != nullptr)
	{
		if (!currentEditor->fitToContent) r.setHeight(currentEditor->getHeight());
		currentEditor->setBounds(r);
	}
}

void Inspector::setCurrentInspectable(WeakReference<Inspectable> inspectable)
{
	if (!isEnabled()) return;

	if (inspectable == currentInspectable)
	{
		return;
	}

	if (currentInspectable != nullptr)
	{
		if (!currentInspectable.wasObjectDeleted())
		{
			currentInspectable->removeInspectableListener(this); 
			currentInspectable->setSelected(false);
		}

		if (currentEditor != nullptr)
		{
			vp.setViewedComponent(nullptr);
			currentEditor = nullptr;
		}
	}
	currentInspectable = inspectable;

	if (currentInspectable.get() != nullptr)
	{
		currentInspectable->setSelected(true);
		currentInspectable->addInspectableListener(this);
		currentEditor = currentInspectable->getEditor(true);
	}

	vp.setViewedComponent(currentEditor, false);
	resized();

	listeners.call(&InspectorListener::currentInspectableChanged, this);
}


void Inspector::clear()
{
	setCurrentInspectable(nullptr);
}

void Inspector::inspectableDestroyed(Inspectable * i)
{
	if (currentInspectable == i) setCurrentInspectable(nullptr);
}

void Inspector::currentInspectableSelectionChanged(Inspectable * oldI, Inspectable * newI)
{
	if (newI == nullptr)
	{
		if (oldI == currentInspectable) setCurrentInspectable(nullptr);
	} else
	{
		if (!newI->showInspectorOnSelect) return;
		if (newI->targetInspector == this) setCurrentInspectable(newI);
		else if (newI->targetInspector == nullptr && isMainInspector) setCurrentInspectable(newI);
	}
}

InspectorUI::InspectorUI(bool isMainInspector) :
	ShapeShifterContentComponent("Inspector"),
	inspector(isMainInspector)
{
	addAndMakeVisible(&inspector);
}

InspectorUI::~InspectorUI()
{
}

void InspectorUI::resized()
{
	inspector.setBounds(getLocalBounds());
}

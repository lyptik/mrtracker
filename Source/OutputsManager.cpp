/*
  ==============================================================================

    OutputsManager.cpp
    Created: 23 Feb 2017 10:55:32am
    Author:  Ben

  ==============================================================================
*/

#include "OutputsManager.h"
#include "OutputFactory.h"
#include "SourcesManager.h"

juce_ImplementSingleton(OutputsManager)

OutputsManager::OutputsManager() :
	BaseManager("Outputs")
{
	SourcesManager::getInstance()->addBaseManagerListener(this);
}

OutputsManager::~OutputsManager()
{
	if (SourcesManager::getInstanceWithoutCreating()) SourcesManager::getInstance()->removeBaseManagerListener(this);

	OutputFactory::deleteInstance();
}


void OutputsManager::addItemFromData(var data)
{
	String moduleType = data.getProperty("type", "none");
	if (moduleType.isEmpty()) return;
	Output * i = OutputFactory::getInstance()->createOutput(moduleType);
	if (i != nullptr) addItem(i, data);
}


//From source
void OutputsManager::itemAdded(Source * s)
{
	s->addSourceListener(this);
}

void OutputsManager::itemRemoved(Source * s)
{
	s->removeSourceListener(this);
}

void OutputsManager::sourceTrackableUpdate(Source *, Trackable * t)
{
	for (auto &o : items)
	{
		if (!o->enabled->boolValue()) continue;
		o->sendTrackableData(t);
	}
}

void OutputsManager::sourceTrackableButtonTouch(Source *, Trackable * t, int buttonID, bool value)
{
	for (auto &o : items)
	{
		if (!o->enabled->boolValue()) continue;
		o->sendButtonTouchData(t, buttonID, value); 
	}
}

void OutputsManager::sourceTrackableButtonPress(Source *, Trackable * t, int buttonID, bool value)
{
	for (auto &o : items)
	{
		if (!o->enabled->boolValue()) continue;
		o->sendButtonPressData(t, buttonID, value);
	}
}

void OutputsManager::sourceTrackableAxisUpdate(Source *, Trackable * t, int axisID, float valueX, float valueY)
{
	for (auto &o : items)
	{
		if (!o->enabled->boolValue()) continue;
		o->sendAxisData(t, axisID, valueX, valueY);
	}
}


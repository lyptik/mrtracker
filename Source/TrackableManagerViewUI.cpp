/*
  ==============================================================================

    TrackableManagerViewUI.cpp
    Created: 23 Feb 2017 4:54:49pm
    Author:  Ben

  ==============================================================================
*/

#include "TrackableManagerViewUI.h"
#include "Source.h"

TrackableManagerViewUI::TrackableManagerViewUI(TrackableManager * manager) :
	BaseManagerUI("Trackables",manager,false),
	unitSize(32)
{
	animateItemOnAdd = false;
	setInterceptsMouseClicks(false, false);
	transparentBG = true;
	addExistingItems();

	//constant draw
	startTimerHz(20);
}

TrackableManagerViewUI::~TrackableManagerViewUI()
{

}

void TrackableManagerViewUI::resized()
{
	for (auto &tui : itemsUI)
	{
		updateTrackableUI(tui);
	}
}

void TrackableManagerViewUI::updateTrackableUI(TrackableUI * tui)
{
	if (tui == nullptr) return;
	Point<int> p;
	float tx = tui->item->data.x;
	float tz = tui->item->data.z;
	//DBG("Update trackable UI " << tx << "/" << tz);

	//if (tui->item->source->useRelativePositioning) p = Point<int>(tx*getWidth(),tz*getHeight());
	//else 
		
	p = Point<int>(tx*unitSize, tz*unitSize) + getLocalBounds().getCentre();

	float angle = tui->item->data.oy;

	// dac : protection to prevent crash
	if (!isnan(angle))
	{
		tui->setCentrePosition(p.x, p.y);
		tui->setTransform(AffineTransform().translated(-p.x, -p.y).rotated(degreesToRadians(angle)+float_Pi).translated(p.x, p.y));
	}
	else {
		//NLOG("UI", "ERROR : Angle is equal to Nan, cannot draw correct position");

	}
	

}

TrackableUI * TrackableManagerViewUI::createUIForItem(Trackable * t)
{
	return t->source->getTrackableUI(t);
}

void TrackableManagerViewUI::addItemUIInternal(TrackableUI * tui)
{
	//tui->trackable->addTrackableListener(this);
	//updateTrackableUI(tui);
}

void TrackableManagerViewUI::removeItemUIInternal(TrackableUI * tui)
{
	if (tui->trackable != nullptr)
	{
		//tui->trackable->removeTrackableListener(this);
	}
}

void TrackableManagerViewUI::timerCallback()
{
	for (auto &tui : itemsUI)
	{
		updateTrackableUI(tui);
	}
}

/*
void TrackableManagerViewUI::dataUpdate(Trackable * t)
{
	MessageManagerLock mmLock;
	updateTrackableUI(getUIForItem(t));
}
*/

/*
void TrackableManagerViewUI::newMessage(const Trackable::TrackableAsyncEvent & e)
{
	switch (e.type)
	{
	case Trackable::TrackableAsyncEvent::DATA_UPDATE:
		if(e.trackable != nullptr) updateTrackableUI(getUIForItem(e.trackable));
		break;
	}
}
*/

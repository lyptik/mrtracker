/*
  ==============================================================================

    ViveGenericTracker.cpp
    Created: 7 Apr 2017 2:58:57pm
    Author:  Anakin2

  ==============================================================================
*/

#include "ViveGenericTracker.h"

ViveGenericTracker::ViveGenericTracker(Source * source, uint8 id, const String &prefix) :
	Trackable(source,id,prefix + " "+String(id))
{
	data.type = VIVE_GENERIC_TRACKER_TYPE;
}

  void ViveGenericTracker::updateQuaternion(float x, float y, float z, float w, bool silent)
  {
	  Trackable::updateQuaternion(x, y, z, w, silent);
	  
	  float pitch = -data.oy;
	  float roll = data.ox+90;
	  float yaw = data.oz -90;
	  data.ox = pitch;
	  data.oy = yaw;
	  data.oz = roll;

	 // rotation->setVector(data.ox, data.oy, data.oz);

	  //DBG(data.oy << " / " << data.ox << " / " << data.oz);
	  
	  return;

	  /*

	  data.qx = x;
	  data.qy = y;
	  data.qz = z;
	  data.qw = w;

	  //Inverse axis for generic
	  float yaw = radiansToDegrees(atan2f(2 * y*w - 2 * x*z, 1 - 2 * y*y - 2 * z*z));
	  float pitch = -radiansToDegrees(atan2f(2 * x*w - 2 * y*z, 1 - 2 * x*x - 2 * z*z))-90;
	  float roll = -radiansToDegrees(asinf(2 * x*y + 2 * z*w));

	  updateRotation(pitch, yaw, roll);

	  if (!silent) dispatchDataUpdate();
	  */
  }

/*
  ==============================================================================

    ViveLightHouse.cpp
    Created: 22 Mar 2017 3:37:49pm
    Author:  Ben

  ==============================================================================
*/

#include "ViveLightHouse.h"

ViveLightHouse::ViveLightHouse(Source * source, uint8 id) :
	ViveGenericTracker(source, id, "Lighthouse ")
{
	data.type = VIVE_LIGHTHOUSE_TYPE;
}
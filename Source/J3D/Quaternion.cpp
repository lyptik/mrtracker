#include "Quaternion.h"
#include "Matrix4f.h"

JQuaternion::JQuaternion(float x, float y, float z, float w)
{
	this->m_x = x;
	this->m_y = y;
	this->m_z = z;
	this->m_w = w;
}

JQuaternion::JQuaternion(Vector3f axis, float angle)
{
	float sinHalfAngle = static_cast<float>(std::sin(angle / 2));
	float cosHalfAngle = static_cast<float>(std::cos(angle / 2));

	this->m_x = axis.GetX() * sinHalfAngle;
	this->m_y = axis.GetY() * sinHalfAngle;
	this->m_z = axis.GetZ() * sinHalfAngle;
	this->m_w = cosHalfAngle;
}

JQuaternion::JQuaternion(Vector3f eulerAngles)
{
	float t0 = cosf(eulerAngles.GetY() * 0.5f);
	float t1 = sinf(eulerAngles.GetY() * 0.5f);
	float t2 = cosf(eulerAngles.GetZ() * 0.5f);
	float t3 = sinf(eulerAngles.GetZ() * 0.5f);
	float t4 = cosf(eulerAngles.GetX() * 0.5f);
	float t5 = sinf(eulerAngles.GetX() * 0.5f);

	Set(
		t0 * t3 * t4 - t1 * t2 * t5, //x
		t0 * t2 * t5 + t1 * t3 * t4, //y
		t1 * t2 * t4 - t0 * t3 * t5, //z
		t0 * t2 * t4 + t1 * t3 * t5  //w
	);
}

float JQuaternion::Length()
{
	return static_cast<float>(std::sqrt(m_x * m_x + m_y * m_y + m_z * m_z + m_w * m_w));
}

JQuaternion JQuaternion::Normalized()
{
	float length = Length();

	return JQuaternion(m_x / length, m_y / length, m_z / length, m_w / length);
}

JQuaternion JQuaternion::Conjugate()
{
	return JQuaternion(-m_x, -m_y, -m_z, m_w);
}

JQuaternion JQuaternion::Mul(float r)
{
	return JQuaternion(m_x * r, m_y * r, m_z * r, m_w * r);
}

JQuaternion JQuaternion::Mul(JQuaternion r)
{
	float w_ = m_w * r.GetW() - m_x * r.GetX() - m_y * r.GetY() - m_z * r.GetZ();
	float x_ = m_x * r.GetW() + m_w * r.GetX() + m_y * r.GetZ() - m_z * r.GetY();
	float y_ = m_y * r.GetW() + m_w * r.GetY() + m_z * r.GetX() - m_x * r.GetZ();
	float z_ = m_z * r.GetW() + m_w * r.GetZ() + m_x * r.GetY() - m_y * r.GetX();

	return JQuaternion(x_, y_, z_, w_);
}

JQuaternion JQuaternion::Mul(Vector3f r)
{
	float w_ = -m_x * r.GetX() - m_y * r.GetY() - m_z * r.GetZ();
	float x_ = m_w * r.GetX() + m_y * r.GetZ() - m_z * r.GetY();
	float y_ = m_w * r.GetY() + m_z * r.GetX() - m_x * r.GetZ();
	float z_ = m_w * r.GetZ() + m_x * r.GetY() - m_y * r.GetX();

	return JQuaternion(x_, y_, z_, w_);
}

JQuaternion JQuaternion::Sub(JQuaternion r)
{
	return JQuaternion(m_x - r.GetX(), m_y - r.GetY(), m_z - r.GetZ(), m_w - r.GetW());
}

JQuaternion JQuaternion::Add(JQuaternion r)
{
	return JQuaternion(m_x + r.GetX(), m_y + r.GetY(), m_z + r.GetZ(), m_w + r.GetW());
}

Matrix4f JQuaternion::ToRotationMatrix()
{
	Vector3f forward(2.0f * (m_x * m_z - m_w * m_y), 2.0f * (m_y * m_z + m_w * m_x), 1.0f - 2.0f * (m_x * m_x + m_y * m_y));
	Vector3f up(2.0f * (m_x * m_y + m_w * m_z), 1.0f - 2.0f * (m_x * m_x + m_z * m_z), 2.0f * (m_y * m_z - m_w * m_x));
	Vector3f right(1.0f - 2.0f * (m_y * m_y + m_z * m_z), 2.0f * (m_x * m_y - m_w * m_z), 2.0f * (m_x * m_z + m_w * m_y));

	Matrix4f m; 
	m.InitRotation(forward, up, right);
	return m;
}

float JQuaternion::Dot(JQuaternion r)
{
	return m_x * r.GetX() + m_y * r.GetY() + m_z * r.GetZ() + m_w * r.GetW();
}

JQuaternion JQuaternion::NLerp(JQuaternion dest, float lerpFactor, bool shortest)
{
	JQuaternion correctedDest = dest;

	if (shortest && this->Dot(dest) < 0)
	{
		correctedDest = JQuaternion(-dest.GetX(), -dest.GetY(), -dest.GetZ(), -dest.GetW());
	}

	return correctedDest.Sub(*this).Mul(lerpFactor).Add(*this).Normalized();
}

JQuaternion JQuaternion::SLerp(JQuaternion dest, float lerpFactor, bool shortest)
{
	constexpr float EPSILON = 1e3f;

	float cos = this->Dot(dest);
	JQuaternion correctedDest = dest;

	if (shortest && cos < 0)
	{
		cos = -cos;
		correctedDest = JQuaternion(-dest.GetX(), -dest.GetY(), -dest.GetZ(), -dest.GetW());
	}

	if (std::abs(cos) >= 1 - EPSILON)
	{
		return NLerp(correctedDest, lerpFactor, false);
	}

	float sin = static_cast<float>(std::sqrt(1.0f - cos * cos));
	float angle = static_cast<float>(std::atan2(sin, cos));
	float invSin = 1.0f / sin;

	float srcFactor = static_cast<float>(std::sin((1.0f - lerpFactor) * angle)) * invSin;
	float destFactor = static_cast<float>(std::sin((lerpFactor)* angle)) * invSin;

	return Mul(srcFactor).Add(correctedDest.Mul(destFactor));
}

JQuaternion::JQuaternion(Matrix4f rot)
{
	float trace = rot.Get(0, 0) + rot.Get(1, 1) + rot.Get(2, 2);

	if (trace > 0)
	{
		float s = 0.5f / static_cast<float>(std::sqrt(trace + 1.0f));
		m_w = 0.25f / s;
		m_x = (rot.Get(1, 2) - rot.Get(2, 1)) * s;
		m_y = (rot.Get(2, 0) - rot.Get(0, 2)) * s;
		m_z = (rot.Get(0, 1) - rot.Get(1, 0)) * s;
	}
	else
	{
		if (rot.Get(0, 0) > rot.Get(1, 1) && rot.Get(0, 0) > rot.Get(2, 2))
		{
			float s = 2.0f * static_cast<float>(std::sqrt(1.0f + rot.Get(0, 0) - rot.Get(1, 1) - rot.Get(2, 2)));
			m_w = (rot.Get(1, 2) - rot.Get(2, 1)) / s;
			m_x = 0.25f * s;
			m_y = (rot.Get(1, 0) + rot.Get(0, 1)) / s;
			m_z = (rot.Get(2, 0) + rot.Get(0, 2)) / s;
		}
		else if (rot.Get(1, 1) > rot.Get(2, 2))
		{
			float s = 2.0f * static_cast<float>(std::sqrt(1.0f + rot.Get(1, 1) - rot.Get(0, 0) - rot.Get(2, 2)));
			m_w = (rot.Get(2, 0) - rot.Get(0, 2)) / s;
			m_x = (rot.Get(1, 0) + rot.Get(0, 1)) / s;
			m_y = 0.25f * s;
			m_z = (rot.Get(2, 1) + rot.Get(1, 2)) / s;
		}
		else
		{
			float s = 2.0f * static_cast<float>(std::sqrt(1.0f + rot.Get(2, 2) - rot.Get(0, 0) - rot.Get(1, 1)));
			m_w = (rot.Get(0, 1) - rot.Get(1, 0)) / s;
			m_x = (rot.Get(2, 0) + rot.Get(0, 2)) / s;
			m_y = (rot.Get(1, 2) + rot.Get(2, 1)) / s;
			m_z = 0.25f * s;
		}
	}

	float length = static_cast<float>(std::sqrt(m_x * m_x + m_y * m_y + m_z * m_z + m_w * m_w));
	m_x /= length;
	m_y /= length;
	m_z /= length;
	m_w /= length;
}

Vector3f JQuaternion::getEuler()
{
	float ysqr = GetY() * GetY();

	// roll (x-axis rotation)
	float t0 = +2 * (GetW() * GetX() + GetY() * GetZ());
	float t1 = +1 - 2 * (GetX() * GetX() + ysqr);

	// pitch (y-axis rotation)
	float t2 = +2 * (GetW() * GetY() - GetZ() * GetX());
	t2 = t2 > 1 ? 1 : t2;
	t2 = t2 < -1 ? -1 : t2;

	// yaw (z-axis rotation)
	float t3 = +2 * (GetW() * GetZ() + GetX() * GetY());
	float t4 = +1 - 2 * (ysqr + GetZ() * GetZ());

	return Vector3f(std::asin(t2), std::atan2(t3, t4), std::atan2(t0, t1));
}

Vector3f JQuaternion::GetForward()
{
	Vector3f tempVar(0, 0, 1);
	return tempVar.Rotate(this);
}

Vector3f JQuaternion::GetBack()
{
	Vector3f tempVar(0, 0, -1);
	return tempVar.Rotate(this);
}

Vector3f JQuaternion::GetUp()
{
	Vector3f tempVar(0, 1, 0);
	return tempVar.Rotate(this);
}

Vector3f JQuaternion::GetDown()
{
	Vector3f tempVar(0, -1, 0);
	return tempVar.Rotate(this);
}

Vector3f JQuaternion::GetRight()
{
	Vector3f tempVar(1, 0, 0);
	return tempVar.Rotate(this);
}

Vector3f JQuaternion::GetLeft()
{
	Vector3f tempVar(-1, 0, 0);
	return tempVar.Rotate(this);
}

JQuaternion *JQuaternion::Set(float x, float y, float z, float w)
{
	this->m_x = x;
	this->m_y = y;
	this->m_z = z;
	this->m_w = w;
	return this;
}

JQuaternion *JQuaternion::Set(JQuaternion r)
{
	Set(r.GetX(), r.GetY(), r.GetZ(), r.GetW());
	return this;
}

float JQuaternion::GetX()
{
	return m_x;
}

void JQuaternion::SetX(float _x)
{
	this->m_x = _x;
}

float JQuaternion::GetY()
{
	return m_y;
}

void JQuaternion::SetY(float _m_y)
{
	this->m_y = _m_y;
}

float JQuaternion::GetZ()
{
	return m_z;
}

void JQuaternion::SetZ(float _z)
{
	this->m_z = _z;
}

float JQuaternion::GetW()
{
	return m_w;
}

void JQuaternion::SetW(float _w)
{
	this->m_w =  _w;
}

bool JQuaternion::equals(JQuaternion r)
{
	return m_x == r.GetX() && m_y == r.GetY() && m_z == r.GetZ() && m_w == r.GetW();
}
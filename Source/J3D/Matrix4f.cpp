#include "Matrix4f.h"
#include "Transform.h"
#include "RectangularVectors.h"

Matrix4f::Matrix4f()
{
	//JAVA TO C++ CONVERTER NOTE: The following call to the 'RectangularVectors' helper class reproduces the rectangular array initialization that is automatic in Java:
	//ORIGINAL LINE: m = new float[4][4];
	m = RectangularVectors::ReturnRectangularFloatVector(4, 4);
}

Matrix4f *Matrix4f::InitIdentity()
{
	m[0][0] = 1;
	m[0][1] = 0;
	m[0][2] = 0;
	m[0][3] = 0;
	m[1][0] = 0;
	m[1][1] = 1;
	m[1][2] = 0;
	m[1][3] = 0;
	m[2][0] = 0;
	m[2][1] = 0;
	m[2][2] = 1;
	m[2][3] = 0;
	m[3][0] = 0;
	m[3][1] = 0;
	m[3][2] = 0;
	m[3][3] = 1;

	return this;
}

Matrix4f *Matrix4f::InitTranslation(float x, float y, float z)
{
	m[0][0] = 1;
	m[0][1] = 0;
	m[0][2] = 0;
	m[0][3] = x;
	m[1][0] = 0;
	m[1][1] = 1;
	m[1][2] = 0;
	m[1][3] = y;
	m[2][0] = 0;
	m[2][1] = 0;
	m[2][2] = 1;
	m[2][3] = z;
	m[3][0] = 0;
	m[3][1] = 0;
	m[3][2] = 0;
	m[3][3] = 1;

	return this;
}

Matrix4f *Matrix4f::InitRotation(float x, float y, float z)
{
	Matrix4f rx;
	Matrix4f ry;
	Matrix4f rz;

	x = static_cast<float>(degreesToRadians(x));
	y = static_cast<float>(degreesToRadians(y));
	z = static_cast<float>(degreesToRadians(z));

	rz.m[0][0] = static_cast<float>(std::cos(z));
	rz.m[0][1] = -static_cast<float>(std::sin(z));
	rz.m[0][2] = 0;
	rz.m[0][3] = 0;
	rz.m[1][0] = static_cast<float>(std::sin(z));
	rz.m[1][1] = static_cast<float>(std::cos(z));
	rz.m[1][2] = 0;
	rz.m[1][3] = 0;
	rz.m[2][0] = 0;
	rz.m[2][1] = 0;
	rz.m[2][2] = 1;
	rz.m[2][3] = 0;
	rz.m[3][0] = 0;
	rz.m[3][1] = 0;
	rz.m[3][2] = 0;
	rz.m[3][3] = 1;

	rx.m[0][0] = 1;
	rx.m[0][1] = 0;
	rx.m[0][2] = 0;
	rx.m[0][3] = 0;
	rx.m[1][0] = 0;
	rx.m[1][1] = static_cast<float>(std::cos(x));
	rx.m[1][2] = -static_cast<float>(std::sin(x));
	rx.m[1][3] = 0;
	rx.m[2][0] = 0;
	rx.m[2][1] = static_cast<float>(std::sin(x));
	rx.m[2][2] = static_cast<float>(std::cos(x));
	rx.m[2][3] = 0;
	rx.m[3][0] = 0;
	rx.m[3][1] = 0;
	rx.m[3][2] = 0;
	rx.m[3][3] = 1;

	ry.m[0][0] = static_cast<float>(std::cos(y));
	ry.m[0][1] = 0;
	ry.m[0][2] = -static_cast<float>(std::sin(y));
	ry.m[0][3] = 0;
	ry.m[1][0] = 0;
	ry.m[1][1] = 1;
	ry.m[1][2] = 0;
	ry.m[1][3] = 0;
	ry.m[2][0] = static_cast<float>(std::sin(y));
	ry.m[2][1] = 0;
	ry.m[2][2] = static_cast<float>(std::cos(y));
	ry.m[2][3] = 0;
	ry.m[3][0] = 0;
	ry.m[3][1] = 0;
	ry.m[3][2] = 0;
	ry.m[3][3] = 1;

	m = rz.Mul(ry.Mul(rx)).GetM();

	return this;
}

Matrix4f *Matrix4f::InitScale(float x, float y, float z)
{
	m[0][0] = x;
	m[0][1] = 0;
	m[0][2] = 0;
	m[0][3] = 0;
	m[1][0] = 0;
	m[1][1] = y;
	m[1][2] = 0;
	m[1][3] = 0;
	m[2][0] = 0;
	m[2][1] = 0;
	m[2][2] = z;
	m[2][3] = 0;
	m[3][0] = 0;
	m[3][1] = 0;
	m[3][2] = 0;
	m[3][3] = 1;

	return this;
}

Matrix4f *Matrix4f::InitPerspective(float fov, float aspectRatio, float zNear, float zFar)
{
	float tanHalfFOV = static_cast<float>(std::tan(fov / 2));
	float zRange = zNear - zFar;

	m[0][0] = 1.0f / (tanHalfFOV * aspectRatio);
	m[0][1] = 0;
	m[0][2] = 0;
	m[0][3] = 0;
	m[1][0] = 0;
	m[1][1] = 1.0f / tanHalfFOV;
	m[1][2] = 0;
	m[1][3] = 0;
	m[2][0] = 0;
	m[2][1] = 0;
	m[2][2] = (-zNear - zFar) / zRange;
	m[2][3] = 2 * zFar * zNear / zRange;
	m[3][0] = 0;
	m[3][1] = 0;
	m[3][2] = 1;
	m[3][3] = 0;


	return this;
}

Matrix4f *Matrix4f::InitOrthographic(float left, float right, float bottom, float top, float near, float far)
{
	float width = right - left;
	float height = top - bottom;
	float depth = far - near;

	m[0][0] = 2 / width;
	m[0][1] = 0;
	m[0][2] = 0;
	m[0][3] = -(right + left) / width;
	m[1][0] = 0;
	m[1][1] = 2 / height;
	m[1][2] = 0;
	m[1][3] = -(top + bottom) / height;
	m[2][0] = 0;
	m[2][1] = 0;
	m[2][2] = -2 / depth;
	m[2][3] = -(far + near) / depth;
	m[3][0] = 0;
	m[3][1] = 0;
	m[3][2] = 0;
	m[3][3] = 1;

	return this;
}

Matrix4f *Matrix4f::InitRotation(Vector3f forward, Vector3f up)
{
	Vector3f f = forward.Normalized();

	Vector3f r = up.Normalized();
	r = r.Cross(f);

	Vector3f u = f.Cross(r);

	return InitRotation(f, u, r);
}

Matrix4f *Matrix4f::InitRotation(Vector3f forward, Vector3f up, Vector3f right)
{
	Vector3f f = forward;
	Vector3f r = right;
	Vector3f u = up;

	m[0][0] = r.GetX();
	m[0][1] = r.GetY();
	m[0][2] = r.GetZ();
	m[0][3] = 0;
	m[1][0] = u.GetX();
	m[1][1] = u.GetY();
	m[1][2] = u.GetZ();
	m[1][3] = 0;
	m[2][0] = f.GetX();
	m[2][1] = f.GetY();
	m[2][2] = f.GetZ();
	m[2][3] = 0;
	m[3][0] = 0;
	m[3][1] = 0;
	m[3][2] = 0;
	m[3][3] = 1;

	return this;
}

Vector3f Matrix4f::Transform(Vector3f r)
{
	return Vector3f(m[0][0] * r.GetX() + m[0][1] * r.GetY() + m[0][2] * r.GetZ() + m[0][3], m[1][0] * r.GetX() + m[1][1] * r.GetY() + m[1][2] * r.GetZ() + m[1][3], m[2][0] * r.GetX() + m[2][1] * r.GetY() + m[2][2] * r.GetZ() + m[2][3]);
}

Matrix4f Matrix4f::Mul(Matrix4f r)
{
	Matrix4f res;

	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			res.Set(i, j, m[i][0] * r.Get(0, j) + m[i][1] * r.Get(1, j) + m[i][2] * r.Get(2, j) + m[i][3] * r.Get(3, j));
		}
	}

	return res;
}

std::vector<std::vector<float>> Matrix4f::GetM()
{
	//JAVA TO C++ CONVERTER NOTE: The following call to the 'RectangularVectors' helper class reproduces the rectangular array initialization that is automatic in Java:
	//ORIGINAL LINE: float[][] res = new float[4][4];
	std::vector<std::vector<float>> res = RectangularVectors::ReturnRectangularFloatVector(4, 4);

	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			res[i][j] = m[i][j];
		}
	}

	return res;
}

float Matrix4f::Get(int x, int y)
{
	return m[x][y];
}

void Matrix4f::SetM(std::vector<std::vector<float>> &_m)
{
	this->m = _m;
}

void Matrix4f::Set(int x, int y, float value)
{
	m[x][y] = value;
}

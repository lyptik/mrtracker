#pragma once

#include "JuceHeader.h"

class Vector2f
{
private:
	float m_x = 0;
	float m_y = 0;

public:
	Vector2f(float x, float y);

	virtual float Length();

	virtual float Max();

	virtual float Dot(Vector2f r);

	virtual Vector2f Normalized();

	virtual float Cross(Vector2f r);

	virtual Vector2f Lerp(Vector2f dest, float lerpFactor);

	virtual Vector2f Rotate(float angle);

	virtual Vector2f Add(Vector2f r);

	virtual Vector2f Add(float r);

	virtual Vector2f Sub(Vector2f r);

	virtual Vector2f Sub(float r);

	virtual Vector2f Mul(Vector2f r);

	virtual Vector2f Mul(float r);

	virtual Vector2f Div(Vector2f r);

	virtual Vector2f Div(float r);

	virtual Vector2f Abs();

	virtual String toString();

	virtual Vector2f * Set(float x, float y);
	virtual Vector2f * Set(Vector2f r);

	virtual float GetX();

	virtual void SetX(float x);

	virtual float GetY();

	virtual void SetY(float y);

	virtual bool equals(Vector2f r);
};
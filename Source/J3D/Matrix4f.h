#pragma once

#include "Vector3f.h"

//JAVA TO C++ CONVERTER NOTE: Forward class declarations:
namespace com { namespace base { namespace engine { namespace core { class Transform; } } } }

/*
 * Copyright (C) 2014 Benny Bobaganoosh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class Matrix4f
{
private:
	std::vector<std::vector<float>> m;

public:
	Matrix4f();

	virtual Matrix4f *InitIdentity();

	virtual Matrix4f *InitTranslation(float x, float y, float z);

	virtual Matrix4f *InitRotation(float x, float y, float z);

	virtual Matrix4f *InitScale(float x, float y, float z);

	virtual Matrix4f *InitPerspective(float fov, float aspectRatio, float zNear, float zFar);

	virtual Matrix4f *InitOrthographic(float left, float right, float bottom, float top, float near, float far);

	virtual Matrix4f *InitRotation(Vector3f forward, Vector3f up);

	virtual Matrix4f *InitRotation(Vector3f forward, Vector3f up, Vector3f right);

	virtual Vector3f Transform(Vector3f r);

	virtual Matrix4f Mul(Matrix4f r);

	virtual std::vector<std::vector<float>> GetM();

	virtual float Get(int x, int y);

	virtual void SetM(std::vector<std::vector<float>> &m);

	virtual void Set(int x, int y, float value);
};
#include "Vector3f.h"
#include "Quaternion.h"

Vector3f::Vector3f(float x, float y, float z)
{
	this->m_x = x;
	this->m_y = y;
	this->m_z = z;
}

float Vector3f::Length()
{
	return static_cast<float>(std::sqrt(m_x * m_x + m_y * m_y + m_z * m_z));
}

float Vector3f::Max()
{
	return std::max(m_x, std::max(m_y, m_z));
}

float Vector3f::Dot(Vector3f r)
{
	return m_x * r.GetX() + m_y * r.GetY() + m_z * r.GetZ();
}

Vector3f Vector3f::Cross(Vector3f r)
{
	float x_ = m_y * r.GetZ() - m_z * r.GetY();
	float y_ = m_z * r.GetX() - m_x * r.GetZ();
	float z_ = m_x * r.GetY() - m_y * r.GetX();

	return Vector3f(x_, y_, z_);
}

Vector3f Vector3f::Normalized()
{
	float length = Length();

	return Vector3f(m_x / length, m_y / length, m_z / length);
}

Vector3f Vector3f::Rotate(Vector3f axis, float angle)
{
	float sinAngle = static_cast<float>(std::sin(-angle));
	float cosAngle = static_cast<float>(std::cos(-angle));

	return this->Cross(axis.Mul(sinAngle)).Add((this->Mul(cosAngle)).Add(axis.Mul(this->Dot(axis.Mul(1 - cosAngle))))); //Rotation on local Y - Rotation on local Z - Rotation on local X
}

Vector3f Vector3f::Rotate(JQuaternion * rotation)
{
	JQuaternion conjugate = rotation->Conjugate();

	JQuaternion w = rotation->Mul(*this).Mul(conjugate);

	return Vector3f(w.GetX(), w.GetY(), w.GetZ());
}

Vector3f Vector3f::Lerp(Vector3f dest, float lerpFactor)
{
	return dest.Sub(*this).Mul(lerpFactor).Add(*this);
}

Vector3f Vector3f::Add(Vector3f r)
{
	return Vector3f(m_x + r.GetX(), m_y + r.GetY(), m_z + r.GetZ());
}

Vector3f Vector3f::Add(float r)
{
	return Vector3f(m_x + r, m_y + r, m_z + r);
}

Vector3f Vector3f::Sub(Vector3f r)
{
	return Vector3f(m_x - r.GetX(), m_y - r.GetY(), m_z - r.GetZ());
}

Vector3f Vector3f::Sub(float r)
{
	return Vector3f(m_x - r, m_y - r, m_z - r);
}

Vector3f Vector3f::Mul(Vector3f r)
{
	return Vector3f(m_x * r.GetX(), m_y * r.GetY(), m_z * r.GetZ());
}

Vector3f Vector3f::Mul(float r)
{
	return Vector3f(m_x * r, m_y * r, m_z * r);
}

Vector3f Vector3f::Div(Vector3f r)
{
	return Vector3f(m_x / r.GetX(), m_y / r.GetY(), m_z / r.GetZ());
}

Vector3f Vector3f::Div(float r)
{
	return Vector3f(m_x / r, m_y / r, m_z / r);
}

Vector3f Vector3f::Abs()
{
	return Vector3f(std::abs(m_x), std::abs(m_y), std::abs(m_z));
}

String Vector3f::toString()
{
	return "(" + String(m_x) + " " + String(m_y) + " " + String(m_z) + ")";
}

Vector2f Vector3f::GetXY()
{
	return Vector2f(m_x, m_y);
}

Vector2f Vector3f::GetYZ()
{
	return Vector2f(m_y, m_z);
}

Vector2f Vector3f::GetZX()
{
	return Vector2f(m_z, m_x);
}

Vector2f Vector3f::GetYX()
{
	return Vector2f(m_y, m_x);
}

Vector2f Vector3f::GetZY()
{
	return Vector2f(m_z, m_y);
}

Vector2f Vector3f::GetXZ()
{
	return Vector2f(m_x, m_z);
}

Vector3f *Vector3f::Set(float x, float y, float z)
{
	this->m_x = x;
	this->m_y = y;
	this->m_z = z;
	return this;
}

Vector3f *Vector3f::Set(Vector3f r)
{
	Set(r.GetX(), r.GetY(), r.GetZ());
	return this;
}

float Vector3f::GetX()
{
	return m_x;
}

void Vector3f::SetX(float x)
{
	this->m_x = x;
}

float Vector3f::GetY()
{
	return m_y;
}

void Vector3f::SetY(float y)
{
	this->m_y = y;
}

float Vector3f::GetZ()
{
	return m_z;
}

void Vector3f::SetZ(float z)
{
	this->m_z = z;
}

bool Vector3f::equals(Vector3f r)
{
	return m_x == r.GetX() && m_y == r.GetY() && m_z == r.GetZ();
}

#pragma once

#include "Vector2f.h"
class JQuaternion;

/* Copyright (C) 2014 Benny Bobaganoosh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
class Vector3f
{
private:
	float m_x = 0;
	float m_y = 0;
	float m_z = 0;

public:
	Vector3f(float x, float y, float z);

	virtual float Length();

	virtual float Max();

	virtual float Dot(Vector3f r);

	virtual Vector3f Cross(Vector3f r);

	virtual Vector3f Normalized();

	virtual Vector3f Rotate(Vector3f axis, float angle);

	virtual Vector3f Rotate(JQuaternion *rotation);

	virtual Vector3f Lerp(Vector3f dest, float lerpFactor);

	virtual Vector3f Add(Vector3f r);

	virtual Vector3f Add(float r);

	virtual Vector3f Sub(Vector3f r);

	virtual Vector3f Sub(float r);

	virtual Vector3f Mul(Vector3f r);

	virtual Vector3f Mul(float r);

	virtual Vector3f Div(Vector3f r);

	virtual Vector3f Div(float r);

	virtual Vector3f Abs();

	virtual String toString();

	virtual Vector2f GetXY();
	virtual Vector2f GetYZ();
	virtual Vector2f GetZX();

	virtual Vector2f GetYX();
	virtual Vector2f GetZY();
	virtual Vector2f GetXZ();

	virtual Vector3f * Set(float x, float y, float z);
	virtual Vector3f * Set(Vector3f r);

	virtual float GetX();

	virtual void SetX(float x);

	virtual float GetY();

	virtual void SetY(float y);

	virtual float GetZ();

	virtual void SetZ(float z);

	virtual bool equals(Vector3f r);
};
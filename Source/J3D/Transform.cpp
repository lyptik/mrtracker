#include "Transform.h"

Transform::Transform() :
	m_pos(0,0,0),
	m_rot(0,0,0,1),
	m_scale(1,1,1),
	m_parent(nullptr)
{
	m_parentMatrix.InitIdentity();
}

Transform::~Transform()
{
}

void Transform::Update()
{
	if (m_oldPos)
	{
		m_oldPos->Set(m_pos);
		m_oldRot->Set(m_rot);
		m_oldScale->Set(m_scale);
	}
	else
	{
		m_oldPos->Set(m_pos.Add(1.0f));
		m_oldRot->Set(m_rot.Mul(0.5f));
		m_oldScale->Set(m_scale.Add(1.0f));
	}
}

void Transform::Rotate(Vector3f axis, float angle)
{
	JQuaternion tempVar(axis, angle);
	m_rot.Set(tempVar.Mul(m_rot).Normalized());
}

void Transform::LookAt(Vector3f point, Vector3f up)
{
	m_rot.Set(GetLookAtRotation(point, up));
}

JQuaternion Transform::GetLookAtRotation(Vector3f point, Vector3f up)
{
	Matrix4f tempVar;
	tempVar.InitRotation(point.Sub(m_pos).Normalized(), up);
	return JQuaternion(tempVar);
}

bool Transform::HasChanged()
{
	if (m_parent != nullptr && m_parent->HasChanged())
	{
		return true;
	}
	if (m_oldPos == nullptr || m_oldRot == nullptr || m_oldScale == nullptr) return true;

	if (!m_pos.equals(*m_oldPos))
	{
		return true;
	}

	if (!m_rot.equals(*m_oldRot))
	{
		return true;
	}

	if (!m_scale.equals(*m_oldScale))
	{
		return true;
	}

	return false;
}

Matrix4f Transform::GetTransformation()
{
	Matrix4f tempVar;
	Matrix4f translationMatrix;
	translationMatrix.InitTranslation(m_pos.GetX(), m_pos.GetY(), m_pos.GetZ());
	Matrix4f rotationMatrix = m_rot.ToRotationMatrix();
	Matrix4f scaleMatrix;
	scaleMatrix.InitScale(m_scale.GetX(), m_scale.GetY(), m_scale.GetZ());

	return GetParentMatrix().Mul(translationMatrix.Mul(rotationMatrix.Mul(scaleMatrix)));
}

Matrix4f Transform::GetParentMatrix()
{
	if (m_parent != nullptr && m_parent->HasChanged())
	{
		m_parentMatrix = m_parent->GetTransformation();
	}

	return m_parentMatrix;
}

void Transform::SetParent(Transform *parent)
{
	this->m_parent = parent;
}

Vector3f Transform::GetTransformedPos()
{
	return GetParentMatrix().Transform(m_pos);
}

JQuaternion Transform::GetTransformedRot()
{
	JQuaternion parentRotation(0, 0, 0, 1);

	if (m_parent != nullptr)
	{
		parentRotation = m_parent->GetTransformedRot();
	}

	return parentRotation.Mul(m_rot);
}

Vector3f Transform::GetPos()
{
	return m_pos;
}

void Transform::SetPos(Vector3f pos)
{
	this->m_pos = pos;
}

JQuaternion Transform::GetRot()
{
	return m_rot;
}

void Transform::SetRot(JQuaternion rotation)
{
	this->m_rot = rotation;
}

Vector3f Transform::GetScale()
{
	return m_scale;
}

void Transform::SetScale(Vector3f scale)
{
	this->m_scale = scale;
}

#pragma once

//JAVA TO C++ CONVERTER NOTE: Forward class declarations:
namespace com { namespace base { namespace engine { namespace core { class Matrix4f; } } } }
namespace com { namespace base { namespace engine { namespace core { class JQuaternion; } } } }

/*
 * Copyright (C) 2014 Benny Bobaganoosh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "Quaternion.h"
#include "Vector3f.h"
#include "Matrix4f.h"

class Transform
{
public:	
	Transform();
	virtual ~Transform();
	
	
	Transform * m_parent;
	Matrix4f m_parentMatrix;

	Vector3f m_pos;
	JQuaternion m_rot;
	Vector3f m_scale;

	ScopedPointer<Vector3f> m_oldPos;
	ScopedPointer<JQuaternion> m_oldRot;
	ScopedPointer<Vector3f> m_oldScale;

	
	
	virtual void Update();

	virtual void Rotate(Vector3f axis, float angle);

	virtual void LookAt(Vector3f point, Vector3f up);

	virtual JQuaternion GetLookAtRotation(Vector3f point, Vector3f up);

	virtual bool HasChanged();

	virtual Matrix4f GetTransformation();

	Matrix4f GetParentMatrix();


	virtual void SetParent(Transform *parent);

	virtual Vector3f GetTransformedPos();

	virtual JQuaternion GetTransformedRot();

	virtual Vector3f GetPos();

	virtual void SetPos(Vector3f pos);

	virtual JQuaternion GetRot();

	virtual void SetRot(JQuaternion rotation);

	virtual Vector3f GetScale();

	virtual void SetScale(Vector3f scale);
};


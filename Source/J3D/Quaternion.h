#pragma once

#include "Vector3f.h"
class Matrix4f;

class JQuaternion
{
private:
	float m_x = 0;
	float m_y = 0;
	float m_z = 0;
	float m_w = 0;

public:
	JQuaternion(float x, float y, float z, float w);

	JQuaternion(Vector3f axis, float angle);

	JQuaternion(Vector3f eulerAngles);

	//From Ken Shoemake's "JQuaternion Calculus and Fast Animation" article
	JQuaternion(Matrix4f rot);

	virtual Vector3f getEuler();

	virtual float Length();

	virtual JQuaternion Normalized();

	virtual JQuaternion Conjugate();

	virtual JQuaternion Mul(float r);

	virtual JQuaternion Mul(JQuaternion r);

	virtual JQuaternion Mul(Vector3f r);

	virtual JQuaternion Sub(JQuaternion r);

	virtual JQuaternion Add(JQuaternion r);

	virtual Matrix4f ToRotationMatrix();

	virtual float Dot(JQuaternion r);

	virtual JQuaternion NLerp(JQuaternion dest, float lerpFactor, bool shortest);

	virtual JQuaternion SLerp(JQuaternion dest, float lerpFactor, bool shortest);

	virtual Vector3f GetForward();

	virtual Vector3f GetBack();

	virtual Vector3f GetUp();

	virtual Vector3f GetDown();

	virtual Vector3f GetRight();

	virtual Vector3f GetLeft();

	virtual JQuaternion * Set(float x, float y, float z, float w);
	virtual JQuaternion * Set(JQuaternion r);

	virtual float GetX();

	virtual void SetX(float x);

	virtual float GetY();

	virtual void SetY(float m_y);

	virtual float GetZ();

	virtual void SetZ(float z);

	virtual float GetW();

	virtual void SetW(float w);

	virtual bool equals(JQuaternion r);
};
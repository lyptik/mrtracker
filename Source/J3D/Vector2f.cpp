#include "Vector2f.h"

Vector2f::Vector2f(float x, float y)
{
	this->m_x = x;
	this->m_y = y;
}

float Vector2f::Length()
{
	return static_cast<float>(std::sqrt(m_x * m_x + m_y * m_y));
}

float Vector2f::Max()
{
	return std::max(m_x, m_y);
}

float Vector2f::Dot(Vector2f r)
{
	return m_x * r.GetX() + m_y * r.GetY();
}

Vector2f Vector2f::Normalized()
{
	float length = Length();

	return Vector2f(m_x / length, m_y / length);
}

float Vector2f::Cross(Vector2f r)
{
	return m_x * r.GetY() - m_y * r.GetX();
}

Vector2f Vector2f::Lerp(Vector2f dest, float lerpFactor)
{
	return dest.Sub(*this).Mul(lerpFactor).Add(*this);
}

Vector2f Vector2f::Rotate(float angle)
{
	double rad = degreesToRadians(angle);
	double cos = std::cos(rad);
	double sin = std::sin(rad);

	return Vector2f(static_cast<float>(m_x * cos - m_y * sin), static_cast<float>(m_x * sin + m_y * cos));
}

Vector2f Vector2f::Add(Vector2f r)
{
	return Vector2f(m_x + r.GetX(), m_y + r.GetY());
}

Vector2f Vector2f::Add(float r)
{
	return Vector2f(m_x + r, m_y + r);
}

Vector2f Vector2f::Sub(Vector2f r)
{
	return Vector2f(m_x - r.GetX(), m_y - r.GetY());
}

Vector2f Vector2f::Sub(float r)
{
	return Vector2f(m_x - r, m_y - r);
}

Vector2f Vector2f::Mul(Vector2f r)
{
	return Vector2f(m_x * r.GetX(), m_y * r.GetY());
}

Vector2f Vector2f::Mul(float r)
{
	return Vector2f(m_x * r, m_y * r);
}

Vector2f Vector2f::Div(Vector2f r)
{
	return Vector2f(m_x / r.GetX(), m_y / r.GetY());
}

Vector2f Vector2f::Div(float r)
{
	return Vector2f(m_x / r, m_y / r);
}

Vector2f Vector2f::Abs()
{
	return Vector2f(std::abs(m_x), std::abs(m_y));
}

String Vector2f::toString()
{
	return "(" + String(m_x) + " " + String(m_y) + ")";
}

Vector2f * Vector2f::Set(float x, float y)
{
	this->m_x = x;
	this->m_y = y;
	return this;
}

Vector2f * Vector2f::Set(Vector2f r)
{
	Set(r.GetX(), r.GetY());
	return this;
}

float Vector2f::GetX()
{
	return m_x;
}

void Vector2f::SetX(float x)
{
	this->m_x = x;
}

float Vector2f::GetY()
{
	return m_y;
}

void Vector2f::SetY(float y)
{
	this->m_y = y;
}

bool Vector2f::equals(Vector2f r)
{
	return m_x == r.GetX() && m_y == r.GetY();
}


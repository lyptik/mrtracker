/*
  ==============================================================================

    TrackableUI.h
    Created: 23 Feb 2017 4:05:23pm
    Author:  Ben

  ==============================================================================
*/

#ifndef TRACKABLEUI_H_INCLUDED
#define TRACKABLEUI_H_INCLUDED

#include "BaseItemMinimalUI.h"
#include "Trackable.h"

class TrackableUI :
	public BaseItemMinimalUI<Trackable>,
	public Trackable::AsyncListener,
	public Timer
{
public:
	TrackableUI(Trackable * t);
	virtual ~TrackableUI();

	Trackable * trackable;
	bool needsRepaint;

	virtual void paint(Graphics &g) override;

	void timerCallback() override;

	void newMessage(const Trackable::TrackableEvent &e) override;

	JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(TrackableUI)
};

#endif  // TRACKABLEUI_H_INCLUDED

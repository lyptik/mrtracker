/*
 ==============================================================================

 Engine.cpp
 Created: 2 Apr 2016 11:03:21am
 Author:  Martin Hermant

 ==============================================================================
 */

#include "Engine.h"
#include "CustomLogger.h"
#include "PresetManager.h"
#include "StringUtil.h"
#include "ControllableFactory.h"
#include "InspectableSelectionManager.h"
#include "SourcesManager.h"
#include "OutputsManager.h"
#include "ScriptUtil.h"

juce_ImplementSingleton(Engine) 

const char* const filenameSuffix = ".mrtracker";
const char* const filenameWildcard = "*.mrtracker";

Engine::Engine() :
	FileBasedDocument(filenameSuffix,
		filenameWildcard,
		"Load a MrTracker file",
		"Save a MrTracker file"),
	ControllableContainer("Root")
{

	skipControllableNameInAddress = true;

	//to move into engine
	Logger::setCurrentLogger(CustomLogger::getInstance());

	InspectableSelectionManager::getInstance(); //selectionManager constructor
}

Engine::~Engine(){

//delete managers
	
  InspectableSelectionManager::deleteInstance();

  SourcesManager::deleteInstance(); 
  OutputsManager::deleteInstance();

  PresetManager::deleteInstance();
  CustomLogger::deleteInstance();
  
  Logger::setCurrentLogger(nullptr);

  ControllableFactory::deleteInstance();

  ScriptUtil::deleteInstance();
  AssetManager::deleteInstance();


}

void Engine::parseCommandline(const String & commandLine){

  for (auto & c:StringUtil::parseCommandLine(commandLine)){

    if(c.command== "f"|| c.command==""){
      if(c.args.size()==0){
        LOG("no file provided for command : "+c.command);
        jassertfalse;
        continue;
      }
      String fileArg = c.args[0];
      if (File::isAbsolutePath(fileArg)) {
        File f(fileArg);
        if (f.existsAsFile()) loadDocument(f);
      }
      else {
        NLOG("Engine","File : " << fileArg << " not found.");
      }
    }

  }

}

void Engine::clear() {



	if (InspectableSelectionManager::getInstanceWithoutCreating())
	{
		InspectableSelectionManager::getInstance()->clearSelection();
		InspectableSelectionManager::getInstance()->setEnabled(false);
	}

	// HERE CLEAR MANAGERS

	PresetManager::getInstance()->clear();
	SourcesManager::getInstance()->clear();
	OutputsManager::getInstance()->clear();

	if (InspectableSelectionManager::getInstanceWithoutCreating()) InspectableSelectionManager::getInstance()->setEnabled(true);

	changed();    //fileDocument
}
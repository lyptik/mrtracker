/*
  ==============================================================================

    OptitrackSource.h
    Created: 23 Feb 2017 12:08:50pm
    Author:  Ben

  ==============================================================================
*/

#ifndef OPTITRACKSOURCE_H_INCLUDED
#define OPTITRACKSOURCE_H_INCLUDED

#include "Source.h"

class OptitrackSource :
	public Source
{
public:
	OptitrackSource(const String &name = "Optitrack");
	~OptitrackSource() {}

	static OptitrackSource * create() { return new OptitrackSource(); }
	virtual String getTypeString() const override { return "Optitrack"; }
};




#endif  // OPTITRACKSOURCE_H_INCLUDED

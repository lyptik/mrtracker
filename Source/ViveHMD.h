/*
  ==============================================================================

    ViveHMD.h
    Created: 6 Mar 2017 11:37:41am
    Author:  Ben-Portable

  ==============================================================================
*/

#ifndef VIVEHMD_H_INCLUDED
#define VIVEHMD_H_INCLUDED

#include "Trackable.h"
#define VIVE_HEADSET_TYPE 0x01


class ViveHMD :
	public Trackable
{
public:
	ViveHMD(Source * source, uint8 id);
	~ViveHMD() {}
};

#endif  // VIVEHMD_H_INCLUDED

/*
  ==============================================================================

    Source.h
    Created: 23 Feb 2017 10:55:27am
    Author:  Ben

  ==============================================================================
*/

#ifndef SOURCE_H_INCLUDED
#define SOURCE_H_INCLUDED

#include "BaseItem.h"
#include "TrackableManager.h"
class TrackableUI;

class Source :
	public BaseItem,
	public BaseManager<Trackable>::Listener,
	public Trackable::TrackableListener,
	public Transform
{
public:
	Source(const String &name = "Source", int sourceTypeID = -1);
	virtual ~Source();

	TrackableManager manager;

	BoolParameter * logIncomingData;
	Trigger * inActivityTrigger;

	Point3DParameter * origin;
	Point3DParameter * scale;
	Point3DParameter * rotation;

	int sourceTypeID; //Type id for source
	bool hasPosition;
	bool hasRotation;
	bool hasSize;
	bool hasCentroid;

	bool useRelativePositioning;

	virtual TrackableUI * getTrackableUI(Trackable * t);

	void itemAdded(Trackable *) override;
	void itemRemoved(Trackable *) override;

	void dataUpdate(Trackable *) override;
	void buttonTouch(Trackable *, int, bool) override;
	void buttonPress(Trackable *, int, bool) override;
	void axisUpdate(Trackable *, int, float, float) override;

	virtual void vibrateController(int /*controllerID*/, float /*strength*/, float /*time*/) {}

	void onContainerParameterChangedInternal(Parameter * p) override;

	class SourceListener {
	public:
		virtual ~SourceListener() {};
		virtual void sourceTrackableUpdate(Source*, Trackable *) {};
		virtual void sourceTrackableButtonTouch(Source*, Trackable *, int, bool) {};
		virtual void sourceTrackableButtonPress(Source*, Trackable *, int, bool) {};
		virtual void sourceTrackableAxisUpdate(Source*, Trackable *, int, float, float) {};
	};

	ListenerList<SourceListener> sourceListeners;
	void addSourceListener(SourceListener* e) { sourceListeners.add(e); }
	void removeSourceListener(SourceListener* e) { sourceListeners.remove(e); }


	var getJSONData() override;
	virtual String getTypeString() const { jassert(false); return ""; } //should be overriden


	JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(Source)
};



#endif  // SOURCE_H_INCLUDED

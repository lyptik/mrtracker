/*
  ==============================================================================

    ViveGenericTracker.h
    Created: 7 Apr 2017 2:58:57pm
    Author:  Anakin2

  ==============================================================================
*/

#ifndef VIVEGENERICTRACKER_H_INCLUDED
#define VIVEGENERICTRACKER_H_INCLUDED

#include "Trackable.h"
#define VIVE_GENERIC_TRACKER_TYPE 0x04


class ViveGenericTracker :
	public Trackable
{
public:
	ViveGenericTracker(Source * source, uint8 id, const String &prexif = "Tracker");
	~ViveGenericTracker() {}


	void updateQuaternion(float x, float y, float z, float w, bool silent = true) override;

};

#endif  // VIVEGENERICTRACKER_H_INCLUDED

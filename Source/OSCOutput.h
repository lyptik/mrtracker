/*
  ==============================================================================

    OSCOutput.h
    Created: 23 Feb 2017 12:13:14pm
    Author:  Ben

  ==============================================================================
*/

#ifndef OSCOUTPUT_H_INCLUDED
#define OSCOUTPUT_H_INCLUDED



#include "Output.h"
#include "Trackable.h"

class OSCOutput :
	public Output,
	public OSCReceiver::Listener<OSCReceiver::RealtimeCallback>
{
public:
	OSCOutput(const String &name = "OSC", int defaultRemotePort = 9000);
	~OSCOutput() {}

	//SEND
	BoolParameter * useLocal;
	StringParameter * remoteHost;
	IntParameter * remotePort;

	IntParameter * localPort;

	OSCSender sender;

	OSCReceiver receiver;
	void setupReceiver();


	

	void setupSender();
	void sendOSC(const OSCMessage &msg);

	void sendTrackableData(Trackable * t) override;
	void sendTrackableMessage(Trackable * t);
	void sendPositionMessage(Trackable * t);
	void sendRotationMessage(Trackable * t);
	void sendCentroidMessage(Trackable * t);
	void sendSizeMessage(Trackable * t);

	void sendButtonTouchData(Trackable * t, int buttonID, bool value) override;
	void sendButtonPressData(Trackable * t, int buttonID, bool value) override;
	void sendAxisData(Trackable * t, int axisID, float valueX, float valueY) override;

	virtual void onContainerParameterChangedInternal(Parameter * p) override;

	static OSCOutput * create() { return new OSCOutput(); }
	virtual String getTypeString() const override { return "OSC"; }


	void processMessage(const OSCMessage &msg);

	// Inherited via Listener
	virtual void oscMessageReceived(const OSCMessage & message) override;
	virtual void oscBundleReceived(const OSCBundle & bundle) override;
};




#endif  // OSCOUTPUT_H_INCLUDED

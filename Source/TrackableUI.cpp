/*
  ==============================================================================

    TrackableUI.cpp
    Created: 23 Feb 2017 4:05:23pm
    Author:  Ben

  ==============================================================================
*/

#include "TrackableUI.h"
#include "Source.h"

TrackableUI::TrackableUI(Trackable * t) :
	BaseItemMinimalUI(t),
	trackable(t),
	needsRepaint(true)
{
	startTimerHz(20);
	setSize(50,50);
	trackable->addAsyncTrackableListener(this);
}

TrackableUI::~TrackableUI()
{
}


void TrackableUI::paint(Graphics & g)
{
	needsRepaint = false;

	g.setColour(Colours::white.withAlpha(.5f));
	g.fillEllipse(getLocalBounds().withSizeKeepingCentre(5,5).toFloat());
	if (trackable->source->hasRotation)
	{ 
		Point<int> p = getLocalBounds().getCentre();
		g.setColour(Colours::orangered);
		g.drawLine((float)p.x,(float)p.y,(float)p.x+ getWidth()/2, (float)p.y, 1);
		g.setColour(Colours::lightblue);
		g.drawLine((float)p.x, (float)p.y, (float)p.x, (float)p.y-getHeight()/2, 1);

	}

	g.setFont(16);
	g.setColour(Colours::white.withAlpha(.7f));
	g.drawText(String(item->data.id), getLocalBounds().toFloat(), Justification::topLeft);
}

void TrackableUI::timerCallback()
{
	if (needsRepaint) repaint();
}

void TrackableUI::newMessage(const Trackable::TrackableEvent & e)
{
	if (e.type == Trackable::TrackableEvent::DETECTION_UPDATE)
	{
		setAlpha(trackable->isDetected->boolValue() ? 1 : .3f);
		needsRepaint = true;
	}
	
}


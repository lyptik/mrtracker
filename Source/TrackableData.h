/*
  ==============================================================================

    TrackableData.h
    Created: 23 Feb 2017 12:24:30pm
    Author:  Ben

  ==============================================================================
*/

#ifndef TRACKABLEDATA_H_INCLUDED
#define TRACKABLEDATA_H_INCLUDED


//Raw data of a trackable, with every property possible
struct TrackableData
{
	//identification
	unsigned char type; //uint8 = 0-255
	unsigned char id;   //uint8 = 0-255

	//position
	float x;
	float y;
	float z;

	//rotation euler
	float ox;
	float oy;
	float oz;

	//rotation quaternion
	float qx;
	float qy;
	float qz;
	float qw;

	//centroid
	float cx;
	float cy;
	float cz;

	//size
	float sx;
	float sy;
	float sz;
};


#endif  // TRACKABLEDATA_H_INCLUDED

/*
  ==============================================================================

    ViveController.h
    Created: 6 Mar 2017 11:37:37am
    Author:  Ben-Portable

  ==============================================================================
*/

#ifndef VIVECONTROLLER_H_INCLUDED
#define VIVECONTROLLER_H_INCLUDED

#include "Trackable.h"

#define VIVE_CONTROLLER_TYPE 0x02

#define MENU_BT_ID 1
#define SIDE_BT_ID 2
#define TRIGGER_BT_ID 3
#define TOUCH

class ViveSource;

class VibrateThread :
	public Thread
{
public:
	VibrateThread(ViveSource * source, int controllerID);
	~VibrateThread();
	 
	ViveSource * source;
	int controllerID;
	float strength;
	float time;

	void vibrate(float strength, float time);

	virtual void run() override;
};


class ViveController : 
	public Trackable
{
public:
	ViveController(Source * source, uint8 id);
	~ViveController() {}

	uint8 id;

	BoolParameter * alternateOrientation;

	BoolParameter * menuBT;
	BoolParameter * triggerBTTouch;
	BoolParameter * triggerBTPressed;
	FloatParameter * triggerValue;
	BoolParameter * sideBT;
	BoolParameter * touchPadTouch;
	BoolParameter * touchPadPressed;
	Point2DParameter * touchPadPos;
	FloatParameter * battery;

	FloatParameter * vibrationLength;
	Trigger * vibrateTrigger;

	VibrateThread vibrateThread;

	void updateQuaternion(float x, float y, float z, float w, bool silent = true) override;
	void handleButtonTouch(int buttonID, bool touch) override;
	void handleButtonPress(int buttonID, bool pressed) override;
	void handleAxisChange(int axisID, float valueX, float valueY) override;

	void vibrate(float strength, float time) override;

	virtual void onContainerParameterChanged(Parameter * p) override;
	virtual void onContainerTriggerTriggered(Trigger * t) override;
};



#endif  // VIVECONTROLLER_H_INCLUDED

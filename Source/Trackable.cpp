/*
  ==============================================================================

	Trackable.cpp
	Created: 23 Feb 2017 11:21:03am
	Author:  Ben

  ==============================================================================
*/

#include "Trackable.h"
#include "Source.h"

Trackable::Trackable(Source * _source, uint8 id,  const String &name) :
	BaseItem(name),
	source(_source),
	queuedNotifier(50)
{
	data.id = id;

	isDetected = addBoolParameter("Is Detected", "Is this tracker detected", true);
	isDetected->isControllableFeedbackOnly = true;

	/*
	position = addPoint3DParameter("Position", "Position");
	position->setBounds(-10, -10, -10, 10, 10, 10);
	position->isControllableFeedbackOnly = true;
	rotation = addPoint3DParameter("Rotation", "rotation");
	rotation->setBounds(-180, -180, -180, 180, 180, 180);
	rotation->isControllableFeedbackOnly = true;
	*/
	SetParent(source);
}

Trackable::~Trackable()
{
	
}

void Trackable::updatePosition(float x, float y, float z, bool silent)
{
	data.x = x;
	data.y = y;
	data.z = z;

	

	if (source->useRelativePositioning)
	{
		data.x = (data.x - .5f) * source->scale->x;
		data.y = (data.y) * source->scale->y;
		data.z = (data.z-.5f) * source->scale->z;

	}
	SetPos(Vector3f(data.x, data.y, data.z));
	
	//position->setVector(data.x, data.y, data.z);
	
	//DBG("World position : " << GetTransformedPos().toString() << " Parent null ?" << m_parent->GetTransformedPos().toString());
	if (!silent) dispatchDataUpdate();
}

void Trackable::updateRotation(float x, float y, float z, bool silent)
{
	data.ox = x;
	data.oy = y;
	data.oz = z;
	//rotation->setVector(data.ox, data.oy, data.oz);

	SetRot(JQuaternion(Vector3f(x, y, z)));
	if (!silent) dispatchDataUpdate();
}

void Trackable::updateQuaternion(float x, float y, float z, float w, bool silent)
{
	data.qx = x;
	data.qy = y;
	data.qz = z;
	data.qw = w;

	float roll = radiansToDegrees(atan2f(2 * y*w - 2 * x*z, 1 - 2 * y*y - 2 * z*z));
	float pitch = radiansToDegrees(atan2f(2 * x*w - 2 * y*z, 1 - 2 * x*x - 2 * z*z));
	float yaw = radiansToDegrees(asinf(2 * x*y + 2 * z*w));

	updateRotation(pitch, yaw, roll);

	if (!silent) dispatchDataUpdate();
}

void Trackable::updateSize(float x, float y, float z, bool silent)
{
	data.sx = x;
	data.sy = y;
	data.sz = z;
	if (!silent) dispatchDataUpdate();
}

void Trackable::updateCentroid(float x, float y, float z, bool silent)
{
	data.cx = x;
	data.cy = y;
	data.cz = z;
	if (!silent) dispatchDataUpdate();
}

void Trackable::handleButtonTouch(int , bool )
{
	//DBG("Trackable button touch " << buttonID << " > " << (int)touch);
}

void Trackable::handleButtonPress(int , bool )
{
	//DBG("Trackable button press " << buttonID << " > " << (int)pressed);
}

void Trackable::handleAxisChange(int , float , float )
{
	//
}

void Trackable::dispatchDataUpdate()
{
	trackableListeners.call(&TrackableListener::dataUpdate, this);
}

void Trackable::dispatchButtonTouch(int buttonID, bool value)
{
	trackableListeners.call(&TrackableListener::buttonTouch, this, buttonID, value);
}

void Trackable::dispatchButtonPress(int buttonID, bool value)
{
	trackableListeners.call(&TrackableListener::buttonPress, this, buttonID, value);
}

void Trackable::dispatchAxisUpdate(int axisID, float valueX, float valueY)
{
	trackableListeners.call(&TrackableListener::axisUpdate, this, axisID, valueX, valueY);
}

Vector3D<float> Trackable::getAbsolutePosition()
{
	//To change here with nesting transforms
	Vector3D<float> pos = Vector3D<float>(data.x,data.y,data.z);
	return pos;
}

Vector3D<float> Trackable::getAbsoluteRotation()
{
	//To change here with nesting transforms
	return Vector3D<float>(data.ox,data.oy,data.oz);
}

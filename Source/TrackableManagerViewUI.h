/*
  ==============================================================================

    TrackableManagerViewUI.h
    Created: 23 Feb 2017 4:54:49pm
    Author:  Ben

  ==============================================================================
*/

#ifndef TRACKABLEMANAGERVIEWUI_H_INCLUDED
#define TRACKABLEMANAGERVIEWUI_H_INCLUDED

#include "BaseManagerUI.h"
#include "TrackableManager.h"
#include "TrackableUI.h"

class TrackableManagerViewUI :
	public BaseManagerUI<TrackableManager, Trackable, TrackableUI>,
	public Trackable::TrackableListener,
	public Timer
{
public:
	TrackableManagerViewUI(TrackableManager * manager);
	~TrackableManagerViewUI();

	float unitSize;

	void resized() override;
	void updateTrackableUI(TrackableUI * tui);

	TrackableUI * createUIForItem(Trackable * t) override;
	void addItemUIInternal(TrackableUI * tui) override;
	void removeItemUIInternal(TrackableUI * tui) override;

	void timerCallback() override;



	//void dataUpdate(Trackable * t) override;
	//void newMessage(const Trackable::TrackableAsyncEvent &e) override;
};



#endif  // TRACKABLEMANAGERVIEWUI_H_INCLUDED

/*
  ==============================================================================

    OutputUI.h
    Created: 23 Feb 2017 11:42:05am
    Author:  Ben

  ==============================================================================
*/

#ifndef OUTPUTUI_H_INCLUDED
#define OUTPUTUI_H_INCLUDED

#include "Output.h"
#include "BaseItemUI.h"

class OutputUI :
	public BaseItemUI<Output>
{
public:
	OutputUI(Output * output);
	~OutputUI();

	JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(OutputUI)
};




#endif  // OUTPUTUI_H_INCLUDED

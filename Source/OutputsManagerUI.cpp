/*
  ==============================================================================

    OutputsManagerUI.cpp
    Created: 23 Feb 2017 10:56:32am
    Author:  Ben

  ==============================================================================
*/

#include "OutputsManagerUI.h"
#include "OutputFactory.h"

OutputsManagerUI::OutputsManagerUI(const String & name, OutputsManager * manager) :
	BaseManagerShapeShifterUI(name, manager, false)
{
}


OutputsManagerUI::~OutputsManagerUI()
{
}


void OutputsManagerUI::showMenuAndAddItem(bool, Point<int>)
{
	Output * m = OutputFactory::getInstance()->showCreateMenu();
	if (m != nullptr) manager->addItem(m);
}


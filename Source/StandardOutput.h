/*
  ==============================================================================

    StandardOutput.h
    Created: 23 Feb 2017 11:35:03am
    Author:  Ben

  ==============================================================================
*/

#ifndef STANDARDOUTPUT_H_INCLUDED
#define STANDARDOUTPUT_H_INCLUDED

#include "Output.h"

#define DATA3D_PACKET_SIZE		14
#define QUATERNION_PACKET_SIZE	18

#define TRACKABLE_PACKET_ID		0x01
#define POSITION_PACKET_ID		0x06
#define CENTROID_PACKET_ID		0x02
#define ROTATION_PACKET_ID		0x04
#define SIZE_PACKET_ID			0x30
#define BUTTON_TOUCH_PACKET_ID	0x40
#define BUTTON_PRESS_PACKET_ID	0x41
#define AXIS_PACKET_ID			0x42
#define PING_PACKET_ID			0x50


class StandardOutput :
	public Output,
	public Thread,
	public Timer
{
public:
	StandardOutput(const String &name = "Standard",int defaultRemotePort = 9000);
	~StandardOutput();

	//SEND
	BoolParameter * useLocal;
	StringParameter * remoteHost;
	IntParameter * remotePort;
	IntParameter * localPort;

	//SEND
	DatagramSocket socket;
	ScopedPointer<DatagramSocket> receiveSocket;

	void sendTrackableData(Trackable * t) override;
	void sendButtonTouchData(Trackable * t, int buttonID, bool value) override;
	void sendButtonPressData(Trackable * t, int buttonID, bool value) override;
	void sendAxisData(Trackable * t, int axisID, float valueX, float valueY) override;
	void sendPing();

	void createTrackablePacket(Trackable * t, MemoryOutputStream * stream);
	void createPositionPacket(Trackable * t, MemoryOutputStream * stream);
	void createRotationPacket(Trackable * t, MemoryOutputStream * stream);
	void createQuaternionPacket(Trackable * t, MemoryOutputStream * stream);
	void createCentroidPacket(Trackable * t, MemoryOutputStream * stream);
	void createSizePacket(Trackable * t, MemoryOutputStream * stream);

	
	void setupReceiver();

	virtual void onContainerParameterChangedInternal(Parameter * p) override;

	static StandardOutput * create() { return new StandardOutput(); }
	virtual String getTypeString() const override { return "Standard"; }


	// Inherited via Thread
	virtual void run() override;

	void processData(const MemoryBlock &m);

	void timerCallback() override;

};



#endif  // STANDARDOUTPUT_H_INCLUDED

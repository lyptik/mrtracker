/*
  ==============================================================================

    SourcesManager.cpp
    Created: 23 Feb 2017 10:55:18am
    Author:  Ben

  ==============================================================================
*/

#include "SourcesManager.h"
#include "SourceFactory.h"

juce_ImplementSingleton(SourcesManager)

SourcesManager::SourcesManager() :
	BaseManager("Sources")
{
}

SourcesManager::~SourcesManager()
{
	SourceFactory::deleteInstance();
}


void SourcesManager::addItemFromData(var data)
{
	String moduleType = data.getProperty("type", "none");
	if (moduleType.isEmpty()) return;
	Source * i = SourceFactory::getInstance()->createSource(moduleType);
	if (i != nullptr) addItem(i, data);
}

void SourcesManager::vibrateController(int controllerID, float strength, float time)
{
	LOG("SM :: vibrateController " << controllerID << " / " << time);
	for (Source * s : items) s->vibrateController(controllerID, strength, time);
}


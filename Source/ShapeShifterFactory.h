/*
  ==============================================================================

    ShapeShifterFactory.h
    Created: 18 May 2016 11:33:09am
    Author:  bkupe

  ==============================================================================
*/

#ifndef SHAPESHIFTERFACTORY_H_INCLUDED
#define SHAPESHIFTERFACTORY_H_INCLUDED

#include "JuceHeader.h"//keep

class ShapeShifterContent;


const static StringArray globalPanelNames = { "Inspector","Logger","Sources","Outputs","2D View"};
enum PanelName {InspectorPanel, LoggerPanel, SourcesPanel,OutputsPanel,View2DPanel};

class ShapeShifterFactory
{
public:
	static ShapeShifterContent * createContentForIndex(PanelName panelName);
	static ShapeShifterContent * createContentForName(const String &name);
};



#endif  // SHAPESHIFTERFACTORY_H_INCLUDED

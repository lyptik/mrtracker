/*
  ==============================================================================

    ViveLightHouse.h
    Created: 22 Mar 2017 3:37:49pm
    Author:  Ben

  ==============================================================================
*/

#ifndef VIVELIGHTHOUSE_H_INCLUDED
#define VIVELIGHTHOUSE_H_INCLUDED


#include "ViveGenericTracker.h"

#define VIVE_LIGHTHOUSE_TYPE 0x03


class ViveLightHouse :
	public ViveGenericTracker
{
public:
	ViveLightHouse(Source * source, uint8 id);
	~ViveLightHouse() {}

	

};





#endif  // VIVELIGHTHOUSE_H_INCLUDED

/*
  ==============================================================================

    ViveHMD.cpp
    Created: 6 Mar 2017 11:37:41am
    Author:  Ben-Portable

  ==============================================================================
*/

#include "ViveHMD.h"

ViveHMD::ViveHMD(Source * source, uint8 id) :
	Trackable(source, id, "HMD "+String(id))
{
	data.type = VIVE_HEADSET_TYPE;
}

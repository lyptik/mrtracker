/*
  ==============================================================================

    AugmentaSource.cpp
    Created: 23 Feb 2017 11:19:17am
    Author:  Ben

  ==============================================================================
*/

#include "AugmentaSource.h"

AugmentaSource::AugmentaSource(const String & name, int defaultLocalPort, int defaultRemotePort) :
	Source(name,AUGMENTA_SOURCE_ID)
{
	hasSize = true;
	hasCentroid = true;

	//Receive
	useRelativePositioning = true;

	localPort = addIntParameter("Local Port", "Local Port to bind to receive OSC Messages", defaultLocalPort, 1024, 65535);
	localPort->hideInOutliner = true;
	localPort->isTargettable = false;

	isConnected = addBoolParameter("Is Connected", "Is the receiver bound the the local port", false);
	isConnected->isEditable = false;
	isConnected->hideInOutliner = true;
	isConnected->isTargettable = false;
	isConnected->isSavable = false;

	receiver.addListener(this);
	setupReceiver();


	//Send

	useLocal = addBoolParameter("Local", "Send to Local IP (127.0.0.1). Allow to quickly switch between local and remote IP.", true);
	remoteHost = addStringParameter("Remote Host", "Remote Host to send to.", "127.0.0.1");
	remotePort = addIntParameter("Remote port", "Port on which the remote host is listening to", defaultRemotePort, 1024, 65535);

	setupSender();

}

void AugmentaSource::setupReceiver()
{
	bool result = receiver.connect(localPort->intValue());
	isConnected->setValue(result);

	Array<IPAddress> ad;
	IPAddress::findAllAddresses(ad);
	String s = "Local IPs:";
	for (auto &a : ad) s += String("\n > ") + a.toString();
	NLOG(niceName, s);
}

float AugmentaSource::getFloatArg(OSCArgument a)
{
	if (a.isFloat32()) return a.getFloat32();
	if (a.isInt32()) return (float)a.getInt32();
	if (a.isString()) return a.getString().getFloatValue();
	return 0;
}

int AugmentaSource::getIntArg(OSCArgument a)
{
	if (a.isInt32()) return a.getInt32();
	if (a.isFloat32()) return roundToInt(a.getFloat32());
	if (a.isString()) return a.getString().getIntValue();
	return 0;
}

String AugmentaSource::getStringArg(OSCArgument a)
{
	if (a.isString()) return a.getString();
	if (a.isInt32()) return String(a.getInt32());
	if (a.isFloat32()) return String(a.getFloat32());
	return String();
}

void AugmentaSource::processMessage(const OSCMessage & msg)
{
	if (logIncomingData->boolValue())
	{
		String s = "";
		for (auto &a : msg) s += String(" ") + getStringArg(a);
		NLOG(niceName, msg.getAddressPattern().toString() << " :" << s);
	}

	inActivityTrigger->trigger();
	
	//Augmenta specific
	isConnected->setValue(true);

	if (!enabled->boolValue()) return;

	OSCAddressPattern addr = msg.getAddressPattern();
	if (addr.matches("/info"))
	{
		updateInfos(msg);
	} else if (addr.matches("/au/scene"))
	{
		sceneCurrentTime = msg[0].getInt32();
		scenePercentCovered = msg[1].getFloat32();
		sceneNumPeople = msg[2].getInt32();
		sceneAverageMotion = Point<float>(msg[3].getFloat32(), msg[4].getFloat32());
		sceneSize = Point<int>(msg[5].getInt32(), msg[6].getInt32());
		sceneDepth = (float)msg[7].getInt32();

		//transformer.sceneRatio->setValue(sceneSize.x*1.f / sceneSize.y);

	} else if (addr.matches("/au/personEntered"))
	{
		if (msg.size() >= 15)
		{
			Trackable * t = manager.addItemWithID((uint8)msg[0].getInt32());
			t->data.type = AUGMENTA_PERSON_TYPE;
			t->updatePosition(msg[3].getFloat32(), 0,msg[4].getFloat32());
			t->updateSize(msg[10].getFloat32(), msg[14].getFloat32(), msg[11].getFloat32());
			t->updateCentroid(msg[3].getFloat32(), 0,msg[4].getFloat32(),false);
		}
	} else if (addr.matches("/au/personWillLeave"))
	{
		manager.removeItemWithID((uint8)msg[0].getInt32());

	} else if (addr.matches("/au/personUpdated"))
	{
		if (msg.size() >= 15)
		{
			Trackable * t = manager.addItemWithID((uint8)msg[0].getInt32()); //if not found, will create one, if already found will return it
			t->data.type = AUGMENTA_PERSON_TYPE;
			t->updatePosition(msg[3].getFloat32(), 0, msg[4].getFloat32());
			t->updateSize(msg[10].getFloat32(), msg[14].getFloat32(), msg[11].getFloat32());
			t->updateCentroid(msg[3].getFloat32(), 0, msg[4].getFloat32(),false);
		}
	}
}



void AugmentaSource::updateInfos(const OSCMessage &m)
{
	remoteHost->setValue(m[0].getString());

	id = m[1].getString();
	macAddressString->setValue(m[2].getString());
	firmwareVersion = m[3].getString();
	settingsFileName = m[4].getString();
	sourceType = m[5].getString();
	planeStatus = m[6].getString();

	LOG("Received info for " << id << "\nHost:Port : " << remoteHost->stringValue() << ":" << remotePort->stringValue() << "\nMac : " << macAddressString->stringValue() << "\nFirmware : " << firmwareVersion << "\n \
		Settings : " << settingsFileName << "\nType : " << sourceType << "\nPlane Status :" << planeStatus);

	setNiceName(id);

	//sourceListeners.call(&AugmentaSourceListener::sourceInfosChanged, this);
	//sendChangeMessage();
}


void AugmentaSource::setupSender()
{
	sender.connect(remoteHost->stringValue(), remotePort->intValue());
}

void AugmentaSource::sendOSC(const OSCMessage & msg)
{
	sender.send(msg);
}


void AugmentaSource::onContainerParameterChangedInternal(Parameter * p)
{
	Source::onContainerParameterChangedInternal(p);
	if (p == localPort) setupReceiver();
	else if (p == remoteHost || p == remotePort) setupSender();
}

void AugmentaSource::oscMessageReceived(const OSCMessage & message)
{
	if (!enabled->boolValue()) return;
	processMessage(message);
}


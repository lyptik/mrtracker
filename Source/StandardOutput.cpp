/*
  ==============================================================================

	StandardOutput.cpp
	Created: 23 Feb 2017 11:35:03am
	Author:  Ben

  ==============================================================================
*/

#include "StandardOutput.h"
#include "SourcesManager.h"

StandardOutput::StandardOutput(const String & name, int defaultRemotePort) :
	Output(name),
	Thread("receive" + name)
{
	//Send

	useLocal = addBoolParameter("Local", "Send to Local IP (127.0.0.1). Allow to quickly switch between local and remote IP.", true);
	remoteHost = addStringParameter("Remote Host", "Remote Host to send to.", "127.0.0.1");
	remotePort = addIntParameter("Remote port", "Port on which the remote host is listening to", defaultRemotePort, 1024, 65535);

	localPort = addIntParameter("Local port", "To receive infos",11500,1024,65535);

	setupReceiver();

	startTimerHz(1);
}

StandardOutput::~StandardOutput()
{
	signalThreadShouldExit();
	waitForThreadToExit(1000);
}



void StandardOutput::sendTrackableData(Trackable * t)
{
	MemoryOutputStream stream;
	createTrackablePacket(t, &stream);
	socket.write(remoteHost->stringValue(), remotePort->intValue(), stream.getData(), (int)stream.getDataSize());
}

void StandardOutput::sendButtonTouchData(Trackable * t, int buttonID, bool value)
{
	MemoryOutputStream stream;
	stream.writeByte(TRACKABLE_PACKET_ID);
	stream.writeByte(13); // 9 bytes for packet def + 4 bytes for button def
	stream.writeByte((uint8_t)t->source->sourceTypeID);
	stream.writeByte(t->data.type);
	stream.writeInt(t->data.id);
	stream.writeByte(1); //only 1 packet


	stream.writeByte(BUTTON_TOUCH_PACKET_ID);
	stream.writeByte(4);  // Size of data in the packet (2 for def + 2 for buttonID + value)
	stream.writeByte((uint8_t)buttonID);
	stream.writeByte((uint8_t)value);

	socket.write(remoteHost->stringValue(), remotePort->intValue(), stream.getData(), (int)stream.getDataSize());
}

void StandardOutput::sendButtonPressData(Trackable * t, int buttonID, bool value)
{
	MemoryOutputStream stream;
	stream.writeByte(TRACKABLE_PACKET_ID);
	stream.writeByte(13); // 9 bytes for packet def + 4 bytes for button def
	stream.writeByte((uint8_t)t->source->sourceTypeID);
	stream.writeByte(t->data.type);
	stream.writeInt(t->data.id);
	stream.writeByte(1); //only 1 packet


	stream.writeByte(BUTTON_PRESS_PACKET_ID);
	stream.writeByte(4);  // Size of data in the packet (2 for def + 2 for buttonID + value)
	stream.writeByte((uint8_t)buttonID);
	stream.writeByte((uint8_t)value);

	socket.write(remoteHost->stringValue(), remotePort->intValue(), stream.getData(), (int)stream.getDataSize());
}

void StandardOutput::sendAxisData(Trackable * t, int axisID, float valueX, float valueY)
{
	MemoryOutputStream stream;
	stream.writeByte(TRACKABLE_PACKET_ID);
	stream.writeByte(13); // 9 bytes for packet def + 4 bytes for button def
	stream.writeByte((uint8_t)t->source->sourceTypeID);
	stream.writeByte(t->data.type);
	stream.writeInt(t->data.id);
	stream.writeByte(1); //only 1 packet


	stream.writeByte(AXIS_PACKET_ID);
	stream.writeByte(11);  // 2 bytes for def + 9 bytes for data
	stream.writeByte((uint8_t)axisID);
	stream.writeFloat(valueX);
	stream.writeFloat(valueY);

	socket.write(remoteHost->stringValue(), remotePort->intValue(), stream.getData(), (int)stream.getDataSize());
}

void StandardOutput::sendPing()
{
	if (!enabled->boolValue()) return;
	MemoryOutputStream stream;
	stream.writeByte(PING_PACKET_ID);
	stream.writeByte(2);  // 2 bytes for data, 0 bytes left
	socket.write(remoteHost->stringValue(), remotePort->intValue(), stream.getData(), (int)stream.getDataSize());
}

void StandardOutput::createTrackablePacket(Trackable * t, MemoryOutputStream * stream)
{
	stream->writeByte(TRACKABLE_PACKET_ID);
	int numSubPackets = 0;
	int packetSize = 9;
	if (t->source->hasPosition) numSubPackets++;
	if (t->source->hasCentroid) numSubPackets++;
	if (t->source->hasRotation) numSubPackets++;
	if (t->source->hasSize) numSubPackets++;
	packetSize += numSubPackets * DATA3D_PACKET_SIZE;
	if (t->source->hasRotation) packetSize += QUATERNION_PACKET_SIZE;

	stream->writeByte((uint8_t)packetSize);
	stream->writeByte((uint8_t)t->source->sourceTypeID);
	stream->writeByte(t->data.type);
	stream->writeInt(t->data.id);
	stream->writeByte((uint8_t)numSubPackets);
	if (t->source->hasPosition) createPositionPacket(t, stream);
	if (t->source->hasCentroid) createCentroidPacket(t, stream);
	if (t->source->hasRotation) createRotationPacket(t, stream);
	if (t->source->hasRotation) createQuaternionPacket(t, stream);
	if (t->source->hasSize) createSizePacket(t, stream);

}

void StandardOutput::createPositionPacket(Trackable * t, MemoryOutputStream * stream)
{
	stream->writeByte(POSITION_PACKET_ID);
	stream->writeByte(DATA3D_PACKET_SIZE);  // Size of data in the packet, after this (so 3*4  for x, y and z + 2 bytes for packet definition)
	/*
	Vector3f v = t->GetTransformedPos();
	stream->writeFloat(v.GetX());
	stream->writeFloat(v.GetY());
	stream->writeFloat(v.GetZ());
	*/
	stream->writeFloat(t->data.x);
	stream->writeFloat(t->data.y);
	stream->writeFloat(t->data.z);
}

void StandardOutput::createRotationPacket(Trackable * t, MemoryOutputStream * stream)
{
	stream->writeByte(ROTATION_PACKET_ID);
	stream->writeByte(DATA3D_PACKET_SIZE); // Size of data in the packet, after this (so 3*4  for x, y and z + 2 bytes for packet definition)
	
	/*Vector3f v = t->GetTransformedRot().getEuler();
	stream->writeFloat(v.GetX());
	stream->writeFloat(v.GetY());
	stream->writeFloat(v.GetZ());
	*/
	//DBG("Pitch, Roll, Yaw : " << t->data.ox << "\t" << t->data.oy << "\t" << t->data.oz);

	stream->writeFloat(t->data.ox);
	stream->writeFloat(t->data.oy);
	stream->writeFloat(t->data.oz);
}

void StandardOutput::createQuaternionPacket(Trackable * t, MemoryOutputStream * stream)
{
	stream->writeByte(ROTATION_PACKET_ID);
	stream->writeByte(QUATERNION_PACKET_SIZE); // Size of data in the packet, after this (so 3, for x, y and z)

										   /*Vector3f v = t->GetTransformedRot().getEuler();
										   stream->writeFloat(v.GetX());
										   stream->writeFloat(v.GetY());
										   stream->writeFloat(v.GetZ());
										   */
	stream->writeFloat(t->data.qx);
	stream->writeFloat(t->data.qy);
	stream->writeFloat(t->data.qz);
	stream->writeFloat(t->data.qw);
}


void StandardOutput::createCentroidPacket(Trackable * t, MemoryOutputStream * stream)
{
	stream->writeByte(ROTATION_PACKET_ID);
	stream->writeByte(DATA3D_PACKET_SIZE); // Size of data in the packet, after this (so 3, for x, y and z)
	
	/*
	Vector3f v = t->GetTransformedPos();
	stream->writeFloat(v.GetX());
	stream->writeFloat(v.GetY());
	stream->writeFloat(v.GetZ());
	*/

	stream->writeFloat(t->data.cx);
	stream->writeFloat(t->data.cy);
	stream->writeFloat(t->data.cz);
}

void StandardOutput::createSizePacket(Trackable * t, MemoryOutputStream * stream)
{
	stream->writeByte(SIZE_PACKET_ID);
	stream->writeByte(DATA3D_PACKET_SIZE); // Size of data in the packet, after this (so 3, for x, y and z)

	/*
	stream->writeFloat(t->data.sx);
	stream->writeFloat(t->data.sy);
	stream->writeFloat(t->data.sz);
	*/
	stream->writeFloat(t->data.sx);
	stream->writeFloat(t->data.sy);
	stream->writeFloat(t->data.sz);
}

void StandardOutput::setupReceiver()
{
	if (isThreadRunning())
	{
		signalThreadShouldExit();
		waitForThreadToExit(2000);
	}

	if (receiveSocket != nullptr) receiveSocket->shutdown();
	receiveSocket = new DatagramSocket();
	bool result = receiveSocket->bindToPort(localPort->intValue());
	if (result) NLOG(niceName, "Receiving on " << localPort->intValue());
	else NLOG(niceName, "Could not bind port " << localPort->intValue());

	startThread();
}

void StandardOutput::onContainerParameterChangedInternal(Parameter * p)
{
	Output::onContainerParameterChangedInternal(p);
	if (p == remoteHost || p == remotePort)
	{
		NLOG(niceName, "Now sending to " + remoteHost->stringValue() + ":" + String(remotePort->intValue()));
	}
	else if (p == localPort)
	{
		setupReceiver();
	}
}

void StandardOutput::run()
{
	char buffer[256];
	MemoryBlock m;
	while (!threadShouldExit())
	{
		sleep(10);
		//DBG("Receive  check");
		if (receiveSocket == nullptr) continue;
		
		int numRead = receiveSocket->read(buffer, 256, false);
		
		
		
		if (numRead > 0)
		{
			DBG("Receive " << numRead);
			for (int i = 0; i < numRead; i++)
			{
				DBG(" > " << (uint8)buffer[i]);
				if ((uint8)buffer[i] == 255)
				{
					processData(m);
					m.setSize(0);
				}
				else
				{
					m.append(buffer+i, 1);
				}
			}
		}
	}
}

void StandardOutput::processData(const MemoryBlock &m)
{
	if (m.getSize() == 0) return;
	MemoryInputStream stream(m, false);
	char command = stream.readByte();
	switch (command)
	{
	case 'v': // vibrate
		LOG("Vibrate !");
		if (m.getSize() >= 10) // 'v', controllerID(1), vibrationStrength (float-4), vibrationTime (float-4)
		{
			int controllerID = (int)stream.readByte();
			float strength = stream.readFloat();
			float time = stream.readFloat();
			SourcesManager::getInstance()->vibrateController(controllerID, strength,time);
		}
		break;
	}
}

void StandardOutput::timerCallback()
{
	sendPing();
}


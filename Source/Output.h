/*
  ==============================================================================

    Output.h
    Created: 23 Feb 2017 10:55:36am
    Author:  Ben

  ==============================================================================
*/

#ifndef OUTPUT_H_INCLUDED
#define OUTPUT_H_INCLUDED

#include "BaseItem.h"
#include "Trackable.h"

class Output :
	public BaseItem
{
public:
	Output(const String &name = "Output");
	virtual ~Output();

	BoolParameter * logOutgoingData;
	
	virtual void sendTrackableData(Trackable * t);
	virtual void sendButtonTouchData(Trackable * t, int buttonID, bool value);
	virtual void sendButtonPressData(Trackable * t, int buttonID, bool value);
	virtual void sendAxisData(Trackable * t, int axisID, float valueX, float valueY);
	
	var getJSONData() override;
	virtual String getTypeString() const { jassert(false); return ""; } //should be overriden


	JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(Output)
};




#endif  // OUTPUT_H_INCLUDED

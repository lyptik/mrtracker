/*
  ==============================================================================

    OutputsManager.h
    Created: 23 Feb 2017 10:55:32am
    Author:  Ben

  ==============================================================================
*/

#ifndef OUTPUTSMANAGER_H_INCLUDED
#define OUTPUTSMANAGER_H_INCLUDED


#include "BaseManager.h"
#include "Output.h"
#include "Source.h"

class OutputsManager :
	public BaseManager<Output>,
	public BaseManager<Source>::Listener,
	public Source::SourceListener
{
public:
	juce_DeclareSingleton(OutputsManager, true)

	OutputsManager();
	~OutputsManager();

	void addItemFromData(var data) override;

	//From source
	void itemAdded(Source * s) override;
	void itemRemoved(Source *s) override;
	
	void sourceTrackableUpdate(Source * s, Trackable *t) override;
	void sourceTrackableButtonTouch(Source * s, Trackable *t, int buttonID, bool value) override;
	void sourceTrackableButtonPress(Source * s, Trackable *t, int buttonID, bool value) override;
	void sourceTrackableAxisUpdate(Source * s, Trackable *t, int axisID, float valueX, float valueY) override;
	JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(OutputsManager)
};





#endif  // OUTPUTSMANAGER_H_INCLUDED

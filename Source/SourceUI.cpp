/*
  ==============================================================================

	SourceUI.cpp
	Created: 23 Feb 2017 11:01:53am
	Author:  Ben

  ==============================================================================
*/

#include "SourceUI.h"
#include "TriggerImageUI.h"

SourceUI::SourceUI(Source * source) :
	BaseItemUI(source)
{
	inActivityUI = source->inActivityTrigger->createImageUI(AssetManager::getInstance()->getInImage());
	inActivityUI->showLabel = false;
	addAndMakeVisible(inActivityUI);

}

SourceUI::~SourceUI()
{
	
}

void SourceUI::resizedInternalHeader(Rectangle<int>& r)
{
	inActivityUI->setBounds(r.removeFromRight(headerHeight));
}
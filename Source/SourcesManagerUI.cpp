/*
  ==============================================================================

    SourcesManagerUI.cpp
    Created: 23 Feb 2017 10:56:26am
    Author:  Ben

  ==============================================================================
*/

#include "SourcesManagerUI.h"
#include "SourceFactory.h"

SourcesManagerUI::SourcesManagerUI(const String & name, SourcesManager * manager) :
	BaseManagerShapeShifterUI(name,manager,false)
{
}


SourcesManagerUI::~SourcesManagerUI()
{
}


void SourcesManagerUI::showMenuAndAddItem(bool, Point<int>)
{
	Source * m = SourceFactory::getInstance()->showCreateMenu();
	if (m != nullptr) manager->addItem(m);
}

/*
  ==============================================================================

    ViveSource.cpp
    Created: 23 Feb 2017 11:19:12am
    Author:  Ben

  ==============================================================================
*/

#include "ViveSource.h"
#include "ViveHMD.h"
#include "ViveController.h"
#include "ViveLightHouse.h"
#include "ViveGenericTracker.h"

ViveSource::ViveSource(const String & name) :
	Source(name,VIVE_SOURCE_ID),
	Thread("vive", 0),
	vrSystem(nullptr)
{

	defaultAlternateOrientation = addBoolParameter("Default Alternate Orientation", "Active the alternate orientation by default for controllers",true);
	hmdPresent = addBoolParameter("HMD is Present", "", false);

	hasRotation = true;

	EVRInitError hmdError = vr::VRInitError_None;
	vrSystem = vr::VR_Init( &hmdError, vr::VRApplication_Other );

	if (hmdError != vr::VRInitError_None )
	{
		vrSystem = NULL;
		NLOG("Vive","Unable to init VR runtime: " << VR_GetVRInitErrorAsEnglishDescription(hmdError) );
		return;
	}
	else
	{
		NLOG("Vive", "Vive is init");
		startThread();
	}

	/*
	DBG("Max tracked device count : " << k_unMaxTrackedDeviceCount);
	for (int i = 0; i < k_unMaxTrackedDeviceCount; i++)
	{
		addTrackableAtIndex(i);
	}
	*/
}

ViveSource::~ViveSource() 
{
	if (vrSystem != nullptr)
	{
		stopThread(100); 
		vrSystem = nullptr;
		VR_Shutdown();
		
	}
}

void ViveSource::addTrackableAtIndex(int vrTrackedDeviceIndex)
{
	ETrackedDeviceClass deviceClass = vrSystem->GetTrackedDeviceClass(vrTrackedDeviceIndex);
	Trackable * t = nullptr;

	switch (deviceClass)
	{
	case TrackedDeviceClass_HMD:
	{
		LOG("Found HMD at index " << vrTrackedDeviceIndex);
		t = new ViveHMD(this, (uint8)vrTrackedDeviceIndex);
		hmdPresent->setValue(true);
	}
	break;

	case TrackedDeviceClass_GenericTracker:
	{
		LOG("Found Generic Tracker at index " << vrTrackedDeviceIndex);
		t = new ViveGenericTracker(this, (uint8)vrTrackedDeviceIndex);
	}
	break;

	case TrackedDeviceClass_Controller:
	{
		LOG("Found Controller at index " << vrTrackedDeviceIndex);
		ViveController * vc = new ViveController(this, (uint8)vrTrackedDeviceIndex);
		vc->alternateOrientation->setValue(defaultAlternateOrientation->boolValue());
		t = vc;
	}
	break;


	case TrackedDeviceClass_TrackingReference:
	{
		LOG("Found LightHouse  at index " << vrTrackedDeviceIndex);
		t = new ViveLightHouse(this, (uint8)vrTrackedDeviceIndex);
	}
	break;


	default:
	{
		LOG("Controller unknown : " << (int)deviceClass);
		t = nullptr;
	}
	break;
	}

	if (t != nullptr)
	{
		trackableMap.set(vrTrackedDeviceIndex, t);
		manager.addItem(t);
	} else
	{
		//trackableIndices[vrTrackedDeviceIndex] = -1;
	}
}

Trackable * ViveSource::getTrackableItemIndex(int vrTrackedDeviceIndex, bool createIfNotThere)
{
	if (trackableMap.contains(vrTrackedDeviceIndex)) return trackableMap[vrTrackedDeviceIndex];

	if(createIfNotThere)
	{
		addTrackableAtIndex(vrTrackedDeviceIndex);
		if (trackableMap.contains(vrTrackedDeviceIndex)) return trackableMap[vrTrackedDeviceIndex];
	}

	return nullptr;
}

Matrix3D<float> ViveSource::getMatrix3DForHMDMatrix(HmdMatrix34_t matPose)
{
	return Matrix3D<float>(
		matPose.m[0][0], matPose.m[1][0], matPose.m[2][0], 0.0,
		matPose.m[0][1], matPose.m[1][1], matPose.m[2][1], 0.0,
		matPose.m[0][2], matPose.m[1][2], matPose.m[2][2], 0.0,
		matPose.m[0][3], matPose.m[1][3], matPose.m[2][3], 1.0f
	);
}

void ViveSource::getRotationFromMatrix(HmdMatrix34_t mm, float & yaw, float & pitch, float & roll)
{
	Matrix3D<float> m = getMatrix3DForHMDMatrix(mm);
	
	yaw = radiansToDegrees(atan2f(mm.m[0][2], mm.m[2][2])) - 90;
	pitch = radiansToDegrees(asinf(-mm.m[1][2]));
	roll = radiansToDegrees(atan2f(mm.m[1][0], mm.m[1][1]));
}

#pragma warning(push)
#pragma warning(disable:4244)
void ViveSource::getQuaternionFromMatrix(HmdMatrix34_t m, float &x, float &y, float &z, float &w)
{
	w = sqrtf(1.0f + m.m[0][0] + m.m[1][1] + m.m[2][2]) / 2.0f;
	float w4 = (4.0f * w);
	x = -(m.m[2][1] - m.m[1][2]) / w4; //inverse x axis
	y = -(m.m[0][2] - m.m[2][0]) / w4; //inverse y axis
	z = (m.m[1][0] - m.m[0][1]) / w4;
}
#pragma warning(pop)


Trackable * ViveSource::getTrackableWithID(int tid)
{
	for (auto &t : manager.items) if (t->data.id == tid) return t;
	return nullptr;
}

void ViveSource::vibrateController(int controllerID, float strength, float time)
{
	Trackable * c = getTrackableWithID(controllerID);
	if (c == nullptr)
	{
		LOG("Trying to vibrate controller " << controllerID << "but doesn't exist");
		return;
	}
	DBG("Vibrate controller " << c->niceName);

	if (c->data.type == (int)VIVE_CONTROLLER_TYPE)
	{
		ViveController * vc = (ViveController *)c;
		if (c != nullptr)
		{
			vc->vibrate(strength, time);
		}
	}
	
}

void ViveSource::vibrateController(int id, float strength)
{
	if (vrSystem == nullptr) return;
	vrSystem->TriggerHapticPulse(id, 0, (unsigned short)(strength * 3000));
}

void ViveSource::run()
{
	while (true)
	{
		VREvent_t vrEvent;
		if (vrSystem == nullptr) break;

		while (vrSystem->PollNextEvent(&vrEvent, sizeof(vrEvent)))
		{
			switch (vrEvent.eventType)
			{
			case vr::VREvent_TrackedDeviceActivated:
			{
				DBG("Device attached :" << (int)vrEvent.trackedDeviceIndex);
				Trackable * t = getTrackableItemIndex(vrEvent.trackedDeviceIndex,true); //Force creation
				if (t != nullptr) t->isDetected->setValue(true);
			}
			break;
			case vr::VREvent_TrackedDeviceDeactivated:
			{
				DBG("Device detached :" << (int)vrEvent.trackedDeviceIndex);
				Trackable * t = getTrackableItemIndex((int)vrEvent.trackedDeviceIndex,false); //
				if (t != nullptr) t->isDetected->setValue(false);

			}
			break;
			case vr::VREvent_TrackedDeviceUpdated:
			{
				DBG("Device updated :" << (int)vrEvent.trackedDeviceIndex);
			}
			break;

			case vr::VREvent_ButtonPress:
			case vr::VREvent_ButtonUnpress:
			{
				DBG("Device button press :" << (int)vrEvent.trackedDeviceIndex);
				Trackable * t = getTrackableItemIndex(vrEvent.trackedDeviceIndex,false);
				if(t != nullptr) t->handleButtonPress(vrEvent.data.controller.button, vrEvent.eventType == vr::VREvent_ButtonPress);
			}
			break;
			
			case vr::VREvent_ButtonTouch:
			case vr::VREvent_ButtonUntouch:
			{
				Trackable * t = getTrackableItemIndex(vrEvent.trackedDeviceIndex,false);
				if (t != nullptr) t->handleButtonTouch(vrEvent.data.controller.button, vrEvent.eventType == vr::VREvent_ButtonTouch);
			}
			break;

			case vr::VREvent_PropertyChanged:
				
				break;

			case vr::VREvent_SceneApplicationChanged:
				DBG("Application changed !");
				break;

			default:
				//DBG("Not handled : " << (int)vrEvent.eventType);
				break;
			}
		}

		vrSystem->GetDeviceToAbsoluteTrackingPose(ETrackingUniverseOrigin::TrackingUniverseStanding, 0, poses, k_unMaxTrackedDeviceCount); //if not +1, last controller is not updated

		
		for (auto &t : manager.items)
		{
			int tIndex = t->data.id;

			DBG("Pose is valid (" << tIndex <<") ? " << (int)(poses[tIndex].bPoseIsValid) << " / " << (int)(poses[tIndex].bDeviceIsConnected));
			if (poses[tIndex].bPoseIsValid && poses[tIndex].bDeviceIsConnected)
			{
				t->isDetected->setValue(true);

				HmdMatrix34_t mat = poses[tIndex].mDeviceToAbsoluteTracking;

				//DBG("Compute pose for " << tIndex);

				//float yaw, pitch, roll;
				//getRotationFromMatrix(mat, yaw, pitch, roll);
				float qx, qy, qz, qw;
				getQuaternionFromMatrix(mat,qx,qy,qz,qw);
				t->updatePosition(mat.m[0][3], mat.m[1][3], mat.m[2][3]);
				//t->updateRotation(-pitch, -(yaw+90), roll);
				t->updateQuaternion(qx,qy,qz,qw);
				
				t->dispatchDataUpdate();


				//DBG("Y P R " << yaw << "/" << pitch << "/" << -roll);

				
			}
			else
			{
				//LOG("Device not connected or pose not valid");
				t->isDetected->setValue(false);
			}

			if (t != nullptr)
			{
				VRControllerState_t st;
				bool result = vrSystem->GetControllerState(tIndex, &st, sizeof(st));
				if (result)
				{
					for (int a = 0; a < 5; a++)
					{
						t->handleAxisChange(a, st.rAxis[a].x, st.rAxis[a].y);
					}
				}
				
				ViveController * vc = dynamic_cast<ViveController *>(t);
				if (vc != nullptr)
				{
					float battery = vrSystem->GetFloatTrackedDeviceProperty(tIndex, Prop_DeviceBatteryPercentage_Float);
					vc->battery->setValue(battery);
				}
			}
			
			
		}

		if (threadShouldExit()) break;
		sleep(10);
	}
	DBG("Vive Thread Finish");
}
 
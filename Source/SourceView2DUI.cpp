/*
  ==============================================================================

    SourceView2DUI.cpp
    Created: 23 Feb 2017 4:49:28pm
    Author:  Ben

  ==============================================================================
*/

#include "SourceView2DUI.h"
#include "BaseManagerUI.h"

SourceView2DUI::SourceView2DUI(Source * _source) :
	BaseItemMinimalUI(_source),
	source(_source)
{
	setInterceptsMouseClicks(true, true);
	trackableManagerUI = new TrackableManagerViewUI(&source->manager);
	addAndMakeVisible(trackableManagerUI);
}

SourceView2DUI::~SourceView2DUI()
{
}

void SourceView2DUI::paint(Graphics & g)
{
	g.setColour(bgColor.withAlpha(.05f));
	g.fillRect(getLocalBounds());
	g.setColour(bgColor);
	g.drawRect(getLocalBounds());

	g.setColour(Colours::white.withAlpha(.4f));
	Point<int> c = getLocalBounds().getCentre();
	g.drawLine(c.x, c.y-4, c.x, c.y+4, 2);
	g.drawLine(c.x-4, c.y, c.x+4, c.y, 2);
}

void SourceView2DUI::resized()
{
	//DBG("Get local bounds " << getLocalBounds().toString());
	trackableManagerUI->setBounds(getLocalBounds());
}



void SourceView2DUI::mouseDown(const MouseEvent & e)
{
	BaseItemMinimalUI::mouseDown(e);
}

void SourceView2DUI::mouseDrag(const MouseEvent & e)
{

}

void SourceView2DUI::controllableFeedbackUpdateInternal(Controllable * c)
{
	if (c == item->origin || c == item->scale || c == item->rotation)
	{
		uiListeners.call(&SourceView2DUIListener::sourceBoundsParamChanged, this);
	}
}

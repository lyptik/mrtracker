/*
  ==============================================================================

    Source.cpp
    Created: 23 Feb 2017 10:55:27am
    Author:  Ben

  ==============================================================================
*/

#include "Source.h"
#include "TrackableUI.h"

Source::Source(const String & name, int _sourceTypeID) :
	BaseItem(name),
	hasPosition(true),
	hasCentroid(false),
	hasRotation(false),
	hasSize(false),
	sourceTypeID(_sourceTypeID),
	manager(this),
	useRelativePositioning(false)
{
	canInspectChildContainers = true;
	
	logIncomingData = addBoolParameter("Log Incoming", "Enable / Disable logging of incoming data for this module", false);
	logIncomingData->hideInOutliner = true;
	logIncomingData->isTargettable = false;


	inActivityTrigger = addTrigger("IN Activity", "Incoming Activity Signal");
	inActivityTrigger->hideInEditor = true;

	origin = addPoint3DParameter("Origin", "Origin for the zero-coord position");
	origin->setBounds(-10, -10, -10, 10, 10, 10);
	scale = addPoint3DParameter("Scale", "Scale factor of the source. Useful for relative-unit based sources like Augmenta");
	scale->setBounds(1,1,1, 20, 20, 20);
	scale->setVector(10,10,10);
	rotation = addPoint3DParameter("Rotation", "Rotation offset of the sources, mainly used to adjust the Y-axis rotation");
	rotation->setBounds(0, 0, 0, 360, 360, 360);

	manager.addBaseManagerListener(this);
	addChildControllableContainer(&manager);
}

Source::~Source()
{

}

TrackableUI * Source::getTrackableUI(Trackable * t) { 
	return new TrackableUI(t); 
}

void Source::itemAdded(Trackable * t)
{
	t->addTrackableListener(this);
	t->SetParent(this);
}

void Source::itemRemoved(Trackable * t)
{
	t->removeTrackableListener(this);
}

void Source::dataUpdate(Trackable * t)
{
	if (!enabled->boolValue()) return;
	sourceListeners.call(&SourceListener::sourceTrackableUpdate, this, t);
}

void Source::buttonTouch(Trackable * t, int buttonID, bool value)
{
	if (!enabled->boolValue()) return;
	sourceListeners.call(&SourceListener::sourceTrackableButtonTouch, this, t, buttonID, value);
}

void Source::buttonPress(Trackable * t, int buttonID, bool value)
{
	if (!enabled->boolValue()) return;
	sourceListeners.call(&SourceListener::sourceTrackableButtonPress, this, t, buttonID, value);
}

void Source::axisUpdate(Trackable * t, int axisID, float valueX, float valueY)
{
	if (!enabled->boolValue()) return;
	sourceListeners.call(&SourceListener::sourceTrackableAxisUpdate, this, t, axisID, valueX, valueY);
}

void Source::onContainerParameterChangedInternal(Parameter * p)
{
	if (p == origin) SetPos(Vector3f(origin->x, origin->y, origin->z));
	else if (p == rotation) SetRot(JQuaternion(Vector3f(degreesToRadians(rotation->x), degreesToRadians(rotation->y), degreesToRadians(rotation->z))));
	//else if (p == scale) SetScale(Vector3f(scale->x, scale->y, scale->z)); //no need i think
}

var Source::getJSONData()
{
	var data = BaseItem::getJSONData();
	data.getDynamicObject()->setProperty("type", getTypeString());
	return data;
}

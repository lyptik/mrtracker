/*
  ==============================================================================

    SourcesManager.h
    Created: 23 Feb 2017 10:55:18am
    Author:  Ben

  ==============================================================================
*/

#ifndef SOURCESMANAGER_H_INCLUDED
#define SOURCESMANAGER_H_INCLUDED

#include "BaseManager.h"
#include "Source.h"

class SourcesManager :
	public BaseManager<Source>
{
public:
	juce_DeclareSingleton(SourcesManager,true)

	SourcesManager();
	~SourcesManager();

	void addItemFromData(var data) override;

	void vibrateController(int controllerID, float strength, float time);

	JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(SourcesManager)
};



#endif  // SOURCESMANAGER_H_INCLUDED

/*
  ==============================================================================

    ViveController.cpp
    Created: 6 Mar 2017 11:37:37am
    Author:  Ben-Portable

  ==============================================================================
*/

#include "ViveController.h"
#include "openvr.h"
#include "ViveSource.h"

ViveController::ViveController(Source * source, uint8 id) :
	Trackable(source,id,"Controller "+String(id)),
	id(id),
	vibrateThread((ViveSource *)source,(int)id)
{
	data.type = VIVE_CONTROLLER_TYPE;

	alternateOrientation = addBoolParameter("Alternate Orientation", "If you need to invert quaternion rotation computation", false);

	menuBT  = addBoolParameter("Menu Button", "Menu button is pressed ?", false);
	triggerBTTouch = addBoolParameter("Trigger Touch", "Trigger is touched ?", false);
	triggerBTPressed = addBoolParameter("Trigger Press", "Trigger is touched ?", false);
	triggerValue = addFloatParameter("Trigger value", "Value of trigger press", 0, 0, 1);
	
	touchPadTouch = addBoolParameter("Touchpad Touch", "Touchpad is touched ?", false);
	touchPadPressed = addBoolParameter("Touchpad Press", "Touchpad is pressed ?", false);
	touchPadPos = addPoint2DParameter("TouchpadPos", "Touchpad 2D Pos");
	touchPadPos->setBounds(-1, -1, 1, 1);
	sideBT = addBoolParameter("Side BT Press", "Side BT (any) is pressed ?", false);


	vibrationLength = addFloatParameter("Vibration Length", "Length of the vibration when triggered", .1f, 0, 1);
	vibrateTrigger = addTrigger("Vibrate", "Make the controller vibrate");

	battery = addFloatParameter("Battery", "Battery of the controller", 0, 0, 1);
	
	menuBT->isEditable = false;
	triggerBTTouch->isEditable = false;
	triggerBTPressed->isEditable = false;
	triggerValue->isEditable = false;
	touchPadTouch->isEditable = false;
	touchPadPressed->isEditable = false;
	touchPadPos->isEditable = false;
	sideBT->isEditable = false;
	battery->isEditable = false;
}


void ViveController::updateQuaternion(float x, float y, float z, float w, bool silent)
{
	Trackable::updateQuaternion(x, y, z, w, silent);

	if (alternateOrientation->boolValue())
	{
		float pitch = -data.oy;
		float roll = data.ox + 90;
		float yaw = data.oz - 90;
		data.ox = pitch;
		data.oy = yaw;
		data.oz = roll;

		//DBG(data.oy << " / " << data.ox << " / " << data.oz);

		return;

		/*

		data.qx = x;
		data.qy = y;
		data.qz = z;
		data.qw = w;

		//Inverse axis for generic
		float yaw = radiansToDegrees(atan2f(2 * y*w - 2 * x*z, 1 - 2 * y*y - 2 * z*z));
		float pitch = -radiansToDegrees(atan2f(2 * x*w - 2 * y*z, 1 - 2 * x*x - 2 * z*z))-90;
		float roll = -radiansToDegrees(asinf(2 * x*y + 2 * z*w));

		updateRotation(pitch, yaw, roll);

		if (!silent) dispatchDataUpdate();
		*/
	}
	
}

void ViveController::handleButtonTouch(int buttonID, bool touch)
{
	Trackable::handleButtonTouch(buttonID, touch);
	switch (buttonID)
	{
	case vr::k_EButton_Axis0:
		touchPadTouch->setValue(touch);
		break;

	case vr::k_EButton_Axis1:
		triggerBTTouch->setValue(touch);
		break;
	}

}

void ViveController::handleButtonPress(int buttonID, bool pressed)
{
	Trackable::handleButtonPress(buttonID, pressed);

	switch (buttonID)
	{
	case vr::k_EButton_Axis0:
		touchPadPressed->setValue(pressed);
		break;

	case vr::k_EButton_Axis1:
		triggerBTPressed->setValue(pressed);
		break;

	case vr::k_EButton_Dashboard_Back:
		sideBT->setValue(pressed);
		break;

	case vr::k_EButton_ApplicationMenu:
		menuBT->setValue(pressed);
		break;
	}
}

void ViveController::handleAxisChange(int axisID, float valueX, float valueY)
{
	Trackable::handleAxisChange(axisID, valueX, valueY);
	switch (axisID)
	{
	case 0: //touchpad
		touchPadPos->setPoint(valueX, valueY);
		break;

	case 1: //trigger
		triggerValue->setValue(valueX);
		break;
	}
}

void ViveController::vibrate(float strength, float time)
{
	
	vibrateThread.vibrate(strength, time);
}

void ViveController::onContainerParameterChanged(Parameter * p)
{
	Trackable::onContainerParameterChanged(p);

	if (p == menuBT) dispatchButtonPress((int)vr::k_EButton_ApplicationMenu, menuBT->boolValue());
	else if (p == triggerBTTouch) dispatchButtonTouch((int)vr::k_EButton_Axis1, triggerBTTouch->boolValue());
	else if (p == triggerBTPressed) dispatchButtonPress((int)vr::k_EButton_Axis1, triggerBTPressed->boolValue());
	else if (p == triggerValue) dispatchAxisUpdate((int)vr::k_EButton_Axis1, triggerValue->floatValue(),0);
	else if (p == touchPadTouch) dispatchButtonTouch((int)vr::k_EButton_Axis0, touchPadTouch->boolValue());
	else if (p == touchPadPressed) dispatchButtonPress((int)vr::k_EButton_Axis0, touchPadPressed->boolValue());
	else if (p == touchPadPos) dispatchAxisUpdate((int)vr::k_EButton_Axis0, touchPadPos->x,touchPadPos->y);
	else if (p == sideBT) dispatchButtonPress((int)vr::k_EButton_Dashboard_Back, sideBT->boolValue());

}

void ViveController::onContainerTriggerTriggered(Trigger * t)
{
	if (t == vibrateTrigger) vibrate(1,vibrationLength->floatValue());
}




VibrateThread::VibrateThread(ViveSource * s, int controllerID) :
	Thread("vibrate" + String(controllerID)),
	source(s),
	controllerID(controllerID)
{
}

VibrateThread::~VibrateThread()
{
}

void VibrateThread::vibrate(float _strength, float _time)
{
	if (isThreadRunning()) return;

	LOG("Vibrate controller " << controllerID);
	this->strength = _strength;
	this->time = _time;

	startThread();
}

void VibrateThread::run()
{
	float timeAtStart = Time::getMillisecondCounter() / 1000.f;
	double curTime = Time::getMillisecondCounter() / 1000.f;

	while (curTime < timeAtStart + time)
	{
		if (threadShouldExit()) return;
		if (source == nullptr) return;

		source->vibrateController(controllerID, strength);

		curTime = Time::getMillisecondCounter() / 1000.f;
	}
}

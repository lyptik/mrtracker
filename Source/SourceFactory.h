/*
  ==============================================================================

    SourceFactory.h
    Created: 23 Feb 2017 11:00:25am
    Author:  Ben

  ==============================================================================
*/

#ifndef SOURCEFACTORY_H_INCLUDED
#define SOURCEFACTORY_H_INCLUDED

#include "Source.h"

class SourceDefinition
{
public:
	String menuPath;
	String sourceType;
	std::function<Source*()> createFunc;

	SourceDefinition(const String &menuPath, const String &type, std::function<Source*()> createFunc) :
		menuPath(menuPath),
		sourceType(type),
		createFunc(createFunc)
	{}
};

class SourceFactory
{
public:
	juce_DeclareSingleton(SourceFactory, true);

	OwnedArray<SourceDefinition> sourceDefs;
	PopupMenu menu;

	SourceFactory();
	~SourceFactory() {}

	void buildPopupMenu();

	static Source* showCreateMenu()
	{
		int result = getInstance()->menu.show();
		if (result == 0) return nullptr;
		else
		{
			SourceDefinition * d = getInstance()->sourceDefs[result - 1];//result 0 is no result
			return d->createFunc();
		}
	}

	static Source * createSource(const String &sourceType)
	{
		for (auto &d : getInstance()->sourceDefs) if (d->sourceType == sourceType) return d->createFunc();
		return nullptr;
	}

	JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(SourceFactory)
};






#endif  // SOURCEFACTORY_H_INCLUDED

/*
  ==============================================================================

    SceneView2D.cpp
    Created: 23 Feb 2017 10:56:05am
    Author:  Ben

  ==============================================================================
*/

#include "SceneView2D.h"
#include "SourcesManager.h"

SceneView2D::SceneView2D() :
	BaseManagerShapeShifterUI("2D View",SourcesManager::getInstance(),false)
{
	contentIsFlexible = true;
	animateItemOnAdd = false;
	addExistingItems();
	addItemBT->setVisible(false);

	setInterceptsMouseClicks(true, true);
}

SceneView2D::~SceneView2D()
{
}

void SceneView2D::paint(Graphics & g)
{
	int checkerSize = pixelsPerUnit;
	int checkerTX = -checkerSize * 2 + ((getWidth() / 2 + viewOffset.x) % (checkerSize * 2));
	int checkerTY = -checkerSize * 2 + ((getHeight() / 2 + viewOffset.y) % (checkerSize * 2));
	Rectangle<int> checkerBounds(checkerTX, checkerTY, getWidth() + checkerSize * 4, getHeight() + checkerSize * 4);
	g.fillCheckerBoard(checkerBounds.toFloat(), checkerSize, checkerSize, BG_COLOR.darker(.3f), BG_COLOR.darker(.2f));

	g.setColour(BG_COLOR.darker(.05f));
	Point<int> center = getSize() / 2;
	center += viewOffset;
	g.drawLine((float)center.x, 0, (float)center.x, (float)getHeight(), 2);
	g.drawLine(0, (float)center.y, (float)getWidth(), (float)center.y, 2);
}

void SceneView2D::resized()
{
	Rectangle<int> r = getLocalBounds();

	for (auto &tui : itemsUI)
	{
		updateSourceUIBounds(tui);
	}
}

void SceneView2D::updateSourceUIBounds(SourceView2DUI * sui)
{
	Vector3D<float> size = sui->item->scale->getVector() * pixelsPerUnit;
	
	Point<int> pe = getPosInView(Point<float>(sui->item->origin->x,sui->item->origin->z));
	Rectangle<int> bounds;
	bounds.setSize(size.x,size.z);
	bounds.setCentre(pe);
	sui->setBounds(bounds);

	float angle = sui->item->rotation->y;
	sui->setTransform(AffineTransform().translated(-pe.x, -pe.y).rotated(degreesToRadians(angle)).translated(pe.x, pe.y));
}

void SceneView2D::setZoom(float ppu)
{
	pixelsPerUnit = ppu;
	for (auto &tui : itemsUI)
	{
		tui->trackableManagerUI->unitSize = pixelsPerUnit;
	}
}

void SceneView2D::homeView()
{
	viewOffset = Point<int>();
	resized();
	repaint();
}

void SceneView2D::sourceBoundsParamChanged(SourceView2DUI * sui)
{
	updateSourceUIBounds(sui);
}


Point<int> SceneView2D::getSize()
{
	return Point<int>(getWidth(), getHeight());
}

Point<float> SceneView2D::getViewMousePosition()
{
	return getViewPos(getMouseXYRelative());
}

Point<float> SceneView2D::getViewPos(const Point<int> &originalPos)
{
	return (originalPos - getSize() / 2 - viewOffset).toFloat() / pixelsPerUnit;
}

Point<int> SceneView2D::getPosInView(const Point<float>& originPos)
{
	return (originPos * pixelsPerUnit).toInt() + (getSize() / 2) + viewOffset;
}


void SceneView2D::addItemUIInternal(SourceView2DUI * sui)
{
	updateSourceUIBounds(sui);
	sui->addSourceViewListener(this);
	sui->addMouseListener(this, false);
}

void SceneView2D::removeItemUIInternal(SourceView2DUI * sui)
{
	sui->removeSourceViewListener(this);
	sui->removeMouseListener(this);
}


void SceneView2D::mouseDown(const MouseEvent & e)
{
	if (e.eventComponent == this)
	{
		if ((e.mods.isLeftButtonDown() && e.mods.isAltDown()) || e.mods.isMiddleButtonDown())
		{
			setMouseCursor(MouseCursor::UpDownLeftRightResizeCursor);
			updateMouseCursor();
			initViewOffset = Point<int>(viewOffset.x, viewOffset.y);
		}
	} else
	{
		SourceView2DUI * sui = static_cast<SourceView2DUI *>(e.eventComponent);
		if (sui != nullptr)
		{
			if (e.mods.isLeftButtonDown())
			{
				initValue = sui->item->origin->getVector();
			} else if (e.mods.isRightButtonDown())
			{
				initValue = sui->item->scale->getVector();
			} else if (e.mods.isMiddleButtonDown())
			{
				initValue = sui->item->rotation->getVector();
			}
		}
	}
	
}

void SceneView2D::mouseDrag(const MouseEvent & e)
{
	if (e.eventComponent == this)
	{
		if ((e.mods.isLeftButtonDown() && e.mods.isAltDown()) || e.mods.isMiddleButtonDown())
		{
			viewOffset = initViewOffset + e.getOffsetFromDragStart();
			resized();
			repaint();
		}
	} else
	{
		SourceView2DUI * sui = static_cast<SourceView2DUI *>(e.eventComponent);
		if (sui != nullptr)
		{
			if (e.mods.isLeftButtonDown())
			{
				Point<float> p = e.getEventRelativeTo(this).getOffsetFromDragStart().toFloat() / pixelsPerUnit;
				sui->item->origin->setVector(initValue+Vector3D<float>(p.x, 0, p.y));
			} else if (e.mods.isRightButtonDown())
			{
				float factor = (e.getOffsetFromDragStart().x / pixelsPerUnit);
				sui->item->scale->setVector(initValue+Vector3D<float>(factor,factor,factor));
			} else if (e.mods.isMiddleButtonDown())
			{
				MouseEvent me = e.getEventRelativeTo(this);
				Point<int> originPoint = getPosInView(Point<float>(sui->item->origin->x, sui->item->origin->y));
				float initAngle = radiansToDegrees(me.getMouseDownPosition().getAngleToPoint(originPoint))+ 180;
				float angle = radiansToDegrees(me.getPosition().getAngleToPoint(originPoint)) + 180;
				float targetAngle = fmod(initValue.y + (angle - initAngle), 360);
				if (targetAngle < 0) targetAngle += 360;
				sui->item->rotation->setVector(Vector3D<float>(0,targetAngle, 0));
			}
			
		}
	}
}


void SceneView2D::mouseUp(const MouseEvent &)
{
	setMouseCursor(MouseCursor::NormalCursor);
	updateMouseCursor();
}

void SceneView2D::mouseWheelMove(const MouseEvent & e, const MouseWheelDetails & d)
{
	setZoom(jmax<int>(pixelsPerUnit + d.deltaY * 10, 10));
	resized();
	repaint();
}

bool SceneView2D::keyPressed(const KeyPress & e)
{
	if (e.getKeyCode() == KeyPress::createFromDescription("h").getKeyCode())
	{
		homeView();
		return true;
	}

	return false;
}


/*
  ==============================================================================

    OutputFactory.cpp
    Created: 23 Feb 2017 11:00:30am
    Author:  Ben

  ==============================================================================
*/

#include "OutputFactory.h"
#include "StandardOutput.h"
#include "OSCOutput.h"
#include "SharedMemoryOutput.h"

juce_ImplementSingleton(OutputFactory)

OutputFactory::OutputFactory() {

	outputDefs.add(new OutputDefinition("", "Standard", &StandardOutput::create));
	outputDefs.add(new OutputDefinition("", "OSC", &OSCOutput::create));
	outputDefs.add(new OutputDefinition("", "SharedMemory", &SharedMemoryOutput::create));
	buildPopupMenu();
}

void OutputFactory::buildPopupMenu()
{
	OwnedArray<PopupMenu> subMenus;
	Array<String> subMenuNames;

	for (auto &d : outputDefs)
	{
		int itemID = outputDefs.indexOf(d) + 1;//start at 1 for menu

		if (d->menuPath.isEmpty())
		{
			menu.addItem(itemID, d->outputType);
			continue;
		}

		int subMenuIndex = -1;

		for (int i = 0; i < subMenus.size(); i++)
		{
			if (subMenuNames[i] == d->menuPath)
			{
				subMenuIndex = i;
				break;
			}
		}
		if (subMenuIndex == -1)
		{
			subMenuNames.add(d->menuPath);
			subMenus.add(new PopupMenu());
			subMenuIndex = subMenus.size() - 1;
		}

		subMenus[subMenuIndex]->addItem(itemID, d->outputType);
	}

	for (int i = 0; i < subMenus.size(); i++) menu.addSubMenu(subMenuNames[i], *subMenus[i]);
}
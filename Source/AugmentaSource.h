/*
  ==============================================================================

    AugmentaSource.h
    Created: 23 Feb 2017 11:19:17am
    Author:  Ben

  ==============================================================================
*/

#ifndef AUGMENTASOURCE_H_INCLUDED
#define AUGMENTASOURCE_H_INCLUDED

#include "Source.h"

#define AUGMENTA_SOURCE_ID 0x01
#define AUGMENTA_PERSON_TYPE 0x01

class AugmentaSource :
	public Source,
	public OSCReceiver::Listener<OSCReceiver::RealtimeCallback>
{
public:
	AugmentaSource(const String &name = "Augmenta", int defaultLocalPort = 5000, int defaultRemotePort = 9000);
	~AugmentaSource() {}

	//RECEIVE
	IntParameter * localPort;
	BoolParameter * isConnected;
	OSCReceiver receiver;

	//SEND
	BoolParameter * useLocal;
	StringParameter * remoteHost;
	IntParameter * remotePort;
	OSCSender sender;

	//Remote informations
	String id;
	StringParameter * macAddressString;
	MACAddress macAddress;
	String firmwareVersion;
	String settingsFileName;
	String planeStatus;
	String sourceType;

	//Scene informations
	int sceneCurrentTime;
	float scenePercentCovered;
	int sceneNumPeople;
	Point<float> sceneAverageMotion;
	Point<int> sceneSize;
	float sceneDepth;

	//RECEIVE
	void setupReceiver();
	float getFloatArg(OSCArgument a);
	int getIntArg(OSCArgument a);
	String getStringArg(OSCArgument a);

	void processMessage(const OSCMessage & msg);
	void updateInfos(const OSCMessage &msg);

	//SEND
	void setupSender();
	void sendOSC(const OSCMessage &msg);

	virtual void onContainerParameterChangedInternal(Parameter * p) override;
	virtual void oscMessageReceived(const OSCMessage & message) override;

	static AugmentaSource * create() { return new AugmentaSource(); }
	virtual String getTypeString() const override { return "Augmenta"; }

};

#endif  // AUGMENTASOURCE_H_INCLUDED
